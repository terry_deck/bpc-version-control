﻿CREATE TABLE [dbo].[c_tblCdErrorType] (
    [ID]          INT           NOT NULL,
    [DESCRIPTION] NVARCHAR (50) NOT NULL,
    [last_chg_dt] DATETIME      NOT NULL,
    CONSTRAINT [PK_c_tblCodeErrorType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

