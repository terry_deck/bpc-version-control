﻿CREATE TABLE [dbo].[dimCategory] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [YEAR1]          NVARCHAR (10) NULL,
    [LOGICTYPE1]     NVARCHAR (1)  NULL,
    [STARTMTH1]      NVARCHAR (3)  NULL,
    [CONSOL_PUSH1]   NVARCHAR (1)  NULL,
    [SEQ2]           NVARCHAR (50) NULL,
    [ID2]            NVARCHAR (20) NULL,
    [CALC2]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION2] NVARCHAR (50) NULL,
    [HLEVEL2]        NVARCHAR (2)  NULL,
    [YEAR2]          NVARCHAR (10) NULL,
    [LOGICTYPE2]     NVARCHAR (1)  NULL,
    [STARTMTH2]      NVARCHAR (3)  NULL,
    [CONSOL_PUSH2]   NVARCHAR (1)  NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimCategory]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimCategory]([ID2] ASC);

