﻿CREATE TABLE [dbo].[aBPC_STAGE] (
    [LEDGER_ID]   NVARCHAR (20)    NULL,
    [COMPANY]     NVARCHAR (20)    NULL,
    [COSTCENTRE]  NVARCHAR (20)    NULL,
    [ACCOUNT]     NVARCHAR (20)    NULL,
    [INTCO]       NVARCHAR (20)    NULL,
    [PROVINCE]    NVARCHAR (20)    NULL,
    [PROGRAM]     NVARCHAR (20)    NULL,
    [PRODUCT]     NVARCHAR (20)    NULL,
    [LOB]         NVARCHAR (20)    NULL,
    [PAR]         NVARCHAR (20)    NULL,
    [PROJECT]     NVARCHAR (20)    NULL,
    [TREATY]      NVARCHAR (20)    NULL,
    [REINSURER]   NVARCHAR (20)    NULL,
    [LOSSYEAR]    NVARCHAR (20)    NULL,
    [A_TYPE]      NVARCHAR (20)    NULL,
    [CURRENCY]    NVARCHAR (20)    NULL,
    [A_FLAG]      NVARCHAR (20)    NULL,
    [PERIOD_NAME] NVARCHAR (20)    NULL,
    [PERIOD_YEAR] NVARCHAR (20)    NULL,
    [PERIOD_NUM]  NVARCHAR (20)    NULL,
    [SOURCE]      NVARCHAR (20)    NULL,
    [DATASRC]     NVARCHAR (20)    NULL,
    [CATEGORY]    NVARCHAR (20)    NULL,
    [TIME]        NVARCHAR (20)    NULL,
    [TIMEID]      NVARCHAR (20)    NULL,
    [SIGNEDDATA]  DECIMAL (25, 10) NULL
);

