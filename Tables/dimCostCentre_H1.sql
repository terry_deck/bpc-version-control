﻿CREATE TABLE [dbo].[dimCostCentre_H1] (
    [SEQ1]            NVARCHAR (50) NULL,
    [ID1]             NVARCHAR (20) NULL,
    [CALC1]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION1]  NVARCHAR (50) NULL,
    [HLEVEL1]         NVARCHAR (2)  NULL,
    [SCALING1]        SMALLINT      NULL,
    [SEQ2]            NVARCHAR (50) NULL,
    [ID2]             NVARCHAR (20) NULL,
    [CALC2]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION2]  NVARCHAR (50) NULL,
    [HLEVEL2]         NVARCHAR (2)  NULL,
    [SCALING2]        SMALLINT      NULL,
    [SEQ3]            NVARCHAR (50) NULL,
    [ID3]             NVARCHAR (20) NULL,
    [CALC3]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION3]  NVARCHAR (50) NULL,
    [HLEVEL3]         NVARCHAR (2)  NULL,
    [SCALING3]        SMALLINT      NULL,
    [SEQ4]            NVARCHAR (50) NULL,
    [ID4]             NVARCHAR (20) NULL,
    [CALC4]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION4]  NVARCHAR (50) NULL,
    [HLEVEL4]         NVARCHAR (2)  NULL,
    [SCALING4]        SMALLINT      NULL,
    [SEQ5]            NVARCHAR (50) NULL,
    [ID5]             NVARCHAR (20) NULL,
    [CALC5]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION5]  NVARCHAR (50) NULL,
    [HLEVEL5]         NVARCHAR (2)  NULL,
    [SCALING5]        SMALLINT      NULL,
    [SEQ6]            NVARCHAR (50) NULL,
    [ID6]             NVARCHAR (20) NULL,
    [CALC6]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION6]  NVARCHAR (50) NULL,
    [HLEVEL6]         NVARCHAR (2)  NULL,
    [SCALING6]        SMALLINT      NULL,
    [SEQ7]            NVARCHAR (50) NULL,
    [ID7]             NVARCHAR (20) NULL,
    [CALC7]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION7]  NVARCHAR (50) NULL,
    [HLEVEL7]         NVARCHAR (2)  NULL,
    [SCALING7]        SMALLINT      NULL,
    [SEQ8]            NVARCHAR (50) NULL,
    [ID8]             NVARCHAR (20) NULL,
    [CALC8]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION8]  NVARCHAR (50) NULL,
    [HLEVEL8]         NVARCHAR (2)  NULL,
    [SCALING8]        SMALLINT      NULL,
    [SEQ9]            NVARCHAR (50) NULL,
    [ID9]             NVARCHAR (20) NULL,
    [CALC9]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION9]  NVARCHAR (50) NULL,
    [HLEVEL9]         NVARCHAR (2)  NULL,
    [SCALING9]        SMALLINT      NULL,
    [SEQ10]           NVARCHAR (50) NULL,
    [ID10]            NVARCHAR (20) NULL,
    [CALC10]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION10] NVARCHAR (50) NULL,
    [HLEVEL10]        NVARCHAR (2)  NULL,
    [SCALING10]       SMALLINT      NULL,
    [SEQ11]           NVARCHAR (50) NULL,
    [ID11]            NVARCHAR (20) NULL,
    [CALC11]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION11] NVARCHAR (50) NULL,
    [HLEVEL11]        NVARCHAR (2)  NULL,
    [SCALING11]       SMALLINT      NULL,
    [SEQ12]           NVARCHAR (50) NULL,
    [ID12]            NVARCHAR (20) NULL,
    [CALC12]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION12] NVARCHAR (50) NULL,
    [HLEVEL12]        NVARCHAR (2)  NULL,
    [SCALING12]       SMALLINT      NULL,
    [SEQ13]           NVARCHAR (50) NULL,
    [ID13]            NVARCHAR (20) NULL,
    [CALC13]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION13] NVARCHAR (50) NULL,
    [HLEVEL13]        NVARCHAR (2)  NULL,
    [SCALING13]       SMALLINT      NULL,
    [ORGANIZATION]    NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimCostCentre_H1]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimCostCentre_H1]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimCostCentre_H1]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimCostCentre_H1]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimCostCentre_H1]([ID5] ASC);


GO
CREATE NONCLUSTERED INDEX [ID6]
    ON [dbo].[dimCostCentre_H1]([ID6] ASC);


GO
CREATE NONCLUSTERED INDEX [ID7]
    ON [dbo].[dimCostCentre_H1]([ID7] ASC);


GO
CREATE NONCLUSTERED INDEX [ID8]
    ON [dbo].[dimCostCentre_H1]([ID8] ASC);


GO
CREATE NONCLUSTERED INDEX [ID9]
    ON [dbo].[dimCostCentre_H1]([ID9] ASC);


GO
CREATE NONCLUSTERED INDEX [ID10]
    ON [dbo].[dimCostCentre_H1]([ID10] ASC);


GO
CREATE NONCLUSTERED INDEX [ID11]
    ON [dbo].[dimCostCentre_H1]([ID11] ASC);


GO
CREATE NONCLUSTERED INDEX [ID12]
    ON [dbo].[dimCostCentre_H1]([ID12] ASC);


GO
CREATE NONCLUSTERED INDEX [ID13]
    ON [dbo].[dimCostCentre_H1]([ID13] ASC);

