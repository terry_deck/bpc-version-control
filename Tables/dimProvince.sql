﻿CREATE TABLE [dbo].[dimProvince] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [SCALING1]       SMALLINT      NULL,
    [SEQ2]           NVARCHAR (50) NULL,
    [ID2]            NVARCHAR (20) NULL,
    [CALC2]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION2] NVARCHAR (50) NULL,
    [HLEVEL2]        NVARCHAR (2)  NULL,
    [SCALING2]       SMALLINT      NULL,
    [SEQ3]           NVARCHAR (50) NULL,
    [ID3]            NVARCHAR (20) NULL,
    [CALC3]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION3] NVARCHAR (50) NULL,
    [HLEVEL3]        NVARCHAR (2)  NULL,
    [SCALING3]       SMALLINT      NULL,
    [SEQ4]           NVARCHAR (50) NULL,
    [ID4]            NVARCHAR (20) NULL,
    [CALC4]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION4] NVARCHAR (50) NULL,
    [HLEVEL4]        NVARCHAR (2)  NULL,
    [SCALING4]       SMALLINT      NULL,
    [SEQ5]           NVARCHAR (50) NULL,
    [ID5]            NVARCHAR (20) NULL,
    [CALC5]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION5] NVARCHAR (50) NULL,
    [HLEVEL5]        NVARCHAR (2)  NULL,
    [SCALING5]       SMALLINT      NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimProvince]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimProvince]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimProvince]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimProvince]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimProvince]([ID5] ASC);

