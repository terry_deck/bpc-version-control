﻿CREATE TABLE [dbo].[c_tblBPC_dw_data] (
    [primary_agent_chf_district] NVARCHAR (300)  NULL,
    [prod]                       NVARCHAR (50)   NULL,
    [acct]                       NVARCHAR (50)   NULL,
    [effective]                  NVARCHAR (6)    NULL,
    [cnt]                        NUMERIC (15, 2) NULL
);

