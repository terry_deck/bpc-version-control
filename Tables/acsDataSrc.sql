﻿CREATE TABLE [dbo].[acsDataSrc] (
    [APP]       NVARCHAR (30)  NOT NULL,
    [ProfileID] NVARCHAR (100) NOT NULL,
    [Member]    NVARCHAR (20)  NOT NULL,
    [RW]        NVARCHAR (1)   NULL
);


GO
CREATE CLUSTERED INDEX [CIDX_DataSrc_01]
    ON [dbo].[acsDataSrc]([ProfileID] ASC, [APP] ASC, [Member] ASC);

