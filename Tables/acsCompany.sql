﻿CREATE TABLE [dbo].[acsCompany] (
    [APP]       NVARCHAR (30)  NOT NULL,
    [ProfileID] NVARCHAR (100) NOT NULL,
    [Member]    NVARCHAR (20)  NOT NULL,
    [RW]        NVARCHAR (1)   NULL
);


GO
CREATE CLUSTERED INDEX [CIDX_Company_01]
    ON [dbo].[acsCompany]([ProfileID] ASC, [APP] ASC, [Member] ASC);

