﻿CREATE TABLE [dbo].[MetaTmpIntCo] (
    [ID]                     NVARCHAR (20) NULL,
    [NEWID]                  NVARCHAR (20) NULL,
    [EVDESCRIPTION]          NVARCHAR (50) NULL,
    [FRDESCRIPTION]          NVARCHAR (50) NULL,
    [PARENTH1]               NVARCHAR (20) NULL,
    [ENTITY]                 NVARCHAR (50) NULL,
    [SCALING]                NVARCHAR (2)  NULL,
    [SHORT_NAME]             NVARCHAR (10) NULL,
    [PAR_CO]                 NVARCHAR (1)  NULL,
    [ENABLE]                 NVARCHAR (1)  NULL,
    [RPTPARENT]              NVARCHAR (20) NULL,
    [CALC]                   NVARCHAR (1)  NULL,
    [DIMCALC]                NVARCHAR (1)  NULL,
    [ISBASEMEM]              NVARCHAR (1)  NULL,
    [HIR]                    NVARCHAR (50) NULL,
    [META_NUM_DISTINCT_RECS] NUMERIC (18)  NULL,
    [autometa_hier_lvl]      INT           NULL
);

