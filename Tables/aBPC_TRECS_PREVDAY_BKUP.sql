USE [PROD_COBPC]
GO

/****** Object:  Table [dbo].[aBPC_TRECS_PREVDAY_BKUP]    Script Date: 01/02/2018 8:25:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aBPC_TRECS_PREVDAY_BKUP](
	[COMPANY] [nvarchar](20) NULL,
	[COSTCENTRE] [nvarchar](20) NULL,
	[ACCOUNT] [nvarchar](20) NULL,
	[INTCO] [nvarchar](20) NULL,
	[PROVINCE] [nvarchar](20) NULL,
	[PROGRAM] [nvarchar](20) NULL,
	[PRODUCT] [nvarchar](20) NULL,
	[LOB] [nvarchar](20) NULL,
	[PAR] [nvarchar](20) NULL,
	[PROJECT] [nvarchar](20) NULL,
	[TREATY] [nvarchar](20) NULL,
	[REINSURER] [nvarchar](20) NULL,
	[LOSSYEAR] [nvarchar](20) NULL,
	[PERIOD] [nvarchar](20) NULL,
	[SIGNEDDATA] [decimal](25, 2) NULL,
	[ACCTYPE] [nvarchar](20) NULL
) ON [PRIMARY]

GO


