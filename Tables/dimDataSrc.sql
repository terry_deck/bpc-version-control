﻿CREATE TABLE [dbo].[dimDataSrc] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [SCALING1]       SMALLINT      NULL,
    [CONSOL_PUSH1]   NVARCHAR (1)  NULL,
    [SEQ2]           NVARCHAR (50) NULL,
    [ID2]            NVARCHAR (20) NULL,
    [CALC2]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION2] NVARCHAR (50) NULL,
    [HLEVEL2]        NVARCHAR (2)  NULL,
    [SCALING2]       SMALLINT      NULL,
    [CONSOL_PUSH2]   NVARCHAR (1)  NULL,
    [SEQ3]           NVARCHAR (50) NULL,
    [ID3]            NVARCHAR (20) NULL,
    [CALC3]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION3] NVARCHAR (50) NULL,
    [HLEVEL3]        NVARCHAR (2)  NULL,
    [SCALING3]       SMALLINT      NULL,
    [CONSOL_PUSH3]   NVARCHAR (1)  NULL,
    [SEQ4]           NVARCHAR (50) NULL,
    [ID4]            NVARCHAR (20) NULL,
    [CALC4]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION4] NVARCHAR (50) NULL,
    [HLEVEL4]        NVARCHAR (2)  NULL,
    [SCALING4]       SMALLINT      NULL,
    [CONSOL_PUSH4]   NVARCHAR (1)  NULL,
    [SEQ5]           NVARCHAR (50) NULL,
    [ID5]            NVARCHAR (20) NULL,
    [CALC5]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION5] NVARCHAR (50) NULL,
    [HLEVEL5]        NVARCHAR (2)  NULL,
    [SCALING5]       SMALLINT      NULL,
    [CONSOL_PUSH5]   NVARCHAR (1)  NULL,
    [SEQ6]           NVARCHAR (50) NULL,
    [ID6]            NVARCHAR (20) NULL,
    [CALC6]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION6] NVARCHAR (50) NULL,
    [HLEVEL6]        NVARCHAR (2)  NULL,
    [SCALING6]       SMALLINT      NULL,
    [CONSOL_PUSH6]   NVARCHAR (1)  NULL,
    [SEQ7]           NVARCHAR (50) NULL,
    [ID7]            NVARCHAR (20) NULL,
    [CALC7]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION7] NVARCHAR (50) NULL,
    [HLEVEL7]        NVARCHAR (2)  NULL,
    [SCALING7]       SMALLINT      NULL,
    [CONSOL_PUSH7]   NVARCHAR (1)  NULL,
    [SEQ8]           NVARCHAR (50) NULL,
    [ID8]            NVARCHAR (20) NULL,
    [CALC8]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION8] NVARCHAR (50) NULL,
    [HLEVEL8]        NVARCHAR (2)  NULL,
    [SCALING8]       SMALLINT      NULL,
    [CONSOL_PUSH8]   NVARCHAR (1)  NULL,
    [SEQ9]           NVARCHAR (50) NULL,
    [ID9]            NVARCHAR (20) NULL,
    [CALC9]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION9] NVARCHAR (50) NULL,
    [HLEVEL9]        NVARCHAR (2)  NULL,
    [SCALING9]       SMALLINT      NULL,
    [CONSOL_PUSH9]   NVARCHAR (1)  NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimDataSrc]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimDataSrc]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimDataSrc]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimDataSrc]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimDataSrc]([ID5] ASC);


GO
CREATE NONCLUSTERED INDEX [ID6]
    ON [dbo].[dimDataSrc]([ID6] ASC);


GO
CREATE NONCLUSTERED INDEX [ID7]
    ON [dbo].[dimDataSrc]([ID7] ASC);


GO
CREATE NONCLUSTERED INDEX [ID8]
    ON [dbo].[dimDataSrc]([ID8] ASC);


GO
CREATE NONCLUSTERED INDEX [ID9]
    ON [dbo].[dimDataSrc]([ID9] ASC);

