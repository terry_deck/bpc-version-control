﻿CREATE TABLE [dbo].[asp_TrialBalanceReport] (
    [ACCOUNTDESC] NVARCHAR (50)    NOT NULL,
    [COMPANY]     NVARCHAR (20)    NOT NULL,
    [COSTCENTRE]  NVARCHAR (20)    NOT NULL,
    [ACCOUNT]     NVARCHAR (20)    NOT NULL,
    [INTCO]       NVARCHAR (20)    NOT NULL,
    [PROVINCE]    NVARCHAR (20)    NOT NULL,
    [PROGRAM]     NVARCHAR (20)    NOT NULL,
    [PRODUCT]     NVARCHAR (20)    NOT NULL,
    [LOB]         NVARCHAR (20)    NOT NULL,
    [PAR]         NVARCHAR (20)    NOT NULL,
    [PROJECT]     NVARCHAR (20)    NOT NULL,
    [TREATY]      NVARCHAR (20)    NOT NULL,
    [REINSURER]   NVARCHAR (20)    NOT NULL,
    [LOSSYEAR]    NVARCHAR (20)    NOT NULL,
    [CATEGORY]    NVARCHAR (20)    NOT NULL,
    [Begining]    DECIMAL (25, 10) NOT NULL,
    [Period]      DECIMAL (25, 10) NOT NULL,
    [YTD]         DECIMAL (25, 10) NOT NULL,
    [TIMEID]      NVARCHAR (20)    NOT NULL
);

