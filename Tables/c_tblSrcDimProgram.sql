﻿CREATE TABLE [dbo].[c_tblSrcDimProgram] (
    [ID]            NVARCHAR (20) NOT NULL,
    [EVDESCRIPTION] NVARCHAR (50) NULL,
    [FRDESCRIPTION] NVARCHAR (50) NULL,
    [PARENTH1]      NVARCHAR (20) NULL,
    [INTCO]         NVARCHAR (20) NULL,
    [ENABLE]        VARCHAR (1)   NULL
);

