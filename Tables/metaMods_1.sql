﻿CREATE TABLE [dbo].[metaMods_1] (
    [mod_type] VARCHAR (3)   NOT NULL,
    [dim_nm]   NVARCHAR (50) NOT NULL,
    [fld_nm]   [sysname]     NULL,
    [hier_lvl] SMALLINT      NULL,
    [id]       NVARCHAR (20) NOT NULL,
    [src_val]  NVARCHAR (50) NULL,
    [dst_val]  NVARCHAR (50) NULL
);

