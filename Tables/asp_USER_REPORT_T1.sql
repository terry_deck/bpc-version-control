﻿CREATE TABLE [dbo].[asp_USER_REPORT_T1] (
    [UserID]      NVARCHAR (100) NOT NULL,
    [FullName]    NVARCHAR (200) NOT NULL,
    [Teams]       NVARCHAR (200) NULL,
    [TaskProfile] NVARCHAR (100) NULL,
    [MAP]         NVARCHAR (100) NULL
);

