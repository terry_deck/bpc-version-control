﻿CREATE TABLE [dbo].[c_tblOraProvinceH1] (
    [ID]            VARCHAR (20) NULL,
    [EVDESCRIPTION] VARCHAR (50) NULL,
    [FRDESCRIPTION] VARCHAR (50) NULL,
    [PARENT]        VARCHAR (20) NULL,
    [ENABLE]        VARCHAR (1)  NULL
);

