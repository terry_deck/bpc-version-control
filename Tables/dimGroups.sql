﻿CREATE TABLE [dbo].[dimGroups] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimGroups]([ID1] ASC);

