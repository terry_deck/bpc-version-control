﻿CREATE TABLE [dbo].[ASP_COPY_DATA1] (
    [CATEGORY]    NVARCHAR (20)    NULL,
    [COMPANY]     NVARCHAR (20)    NULL,
    [DATASRC]     NVARCHAR (20)    NULL,
    [INTCO]       NVARCHAR (20)    NULL,
    [FLOW]        NVARCHAR (20)    NULL,
    [GROUPS]      NVARCHAR (20)    NULL,
    [RPTCURRENCY] NVARCHAR (20)    NULL,
    [LOB]         NVARCHAR (20)    NULL,
    [LOSSYEAR]    NVARCHAR (20)    NULL,
    [PAR]         NVARCHAR (20)    NULL,
    [PRODUCT]     NVARCHAR (20)    NULL,
    [PROVINCE]    NVARCHAR (20)    NULL,
    [REINSURERC]  NVARCHAR (20)    NULL,
    [ACCOUNT]     NVARCHAR (20)    NULL,
    [SIGNEDDATA]  DECIMAL (25, 10) NULL,
    [SOURCE]      TINYINT          NULL,
    [TIMEID]      NVARCHAR (20)    NULL,
    [TREATYC]     NVARCHAR (20)    NULL
);

