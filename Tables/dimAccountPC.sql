﻿CREATE TABLE [dbo].[dimAccountPC] (
    [SEQ1]            NVARCHAR (50) NULL,
    [ID1]             NVARCHAR (20) NULL,
    [ACCTYPE1]        NVARCHAR (3)  NULL,
    [CALC1]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION1]  NVARCHAR (50) NULL,
    [HLEVEL1]         NVARCHAR (2)  NULL,
    [SCALING1]        SMALLINT      NULL,
    [RATETYPE1]       NVARCHAR (20) NULL,
    [FORMULA1]        VARCHAR (600) NULL,
    [MAINACCT1]       NVARCHAR (20) NULL,
    [SOLVEORDER1]     NVARCHAR (20) NULL,
    [SEQ2]            NVARCHAR (50) NULL,
    [ID2]             NVARCHAR (20) NULL,
    [ACCTYPE2]        NVARCHAR (3)  NULL,
    [CALC2]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION2]  NVARCHAR (50) NULL,
    [HLEVEL2]         NVARCHAR (2)  NULL,
    [SCALING2]        SMALLINT      NULL,
    [RATETYPE2]       NVARCHAR (20) NULL,
    [FORMULA2]        VARCHAR (600) NULL,
    [MAINACCT2]       NVARCHAR (20) NULL,
    [SOLVEORDER2]     NVARCHAR (20) NULL,
    [SEQ3]            NVARCHAR (50) NULL,
    [ID3]             NVARCHAR (20) NULL,
    [ACCTYPE3]        NVARCHAR (3)  NULL,
    [CALC3]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION3]  NVARCHAR (50) NULL,
    [HLEVEL3]         NVARCHAR (2)  NULL,
    [SCALING3]        SMALLINT      NULL,
    [RATETYPE3]       NVARCHAR (20) NULL,
    [FORMULA3]        VARCHAR (600) NULL,
    [MAINACCT3]       NVARCHAR (20) NULL,
    [SOLVEORDER3]     NVARCHAR (20) NULL,
    [SEQ4]            NVARCHAR (50) NULL,
    [ID4]             NVARCHAR (20) NULL,
    [ACCTYPE4]        NVARCHAR (3)  NULL,
    [CALC4]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION4]  NVARCHAR (50) NULL,
    [HLEVEL4]         NVARCHAR (2)  NULL,
    [SCALING4]        SMALLINT      NULL,
    [RATETYPE4]       NVARCHAR (20) NULL,
    [FORMULA4]        VARCHAR (600) NULL,
    [MAINACCT4]       NVARCHAR (20) NULL,
    [SOLVEORDER4]     NVARCHAR (20) NULL,
    [SEQ5]            NVARCHAR (50) NULL,
    [ID5]             NVARCHAR (20) NULL,
    [ACCTYPE5]        NVARCHAR (3)  NULL,
    [CALC5]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION5]  NVARCHAR (50) NULL,
    [HLEVEL5]         NVARCHAR (2)  NULL,
    [SCALING5]        SMALLINT      NULL,
    [RATETYPE5]       NVARCHAR (20) NULL,
    [FORMULA5]        VARCHAR (600) NULL,
    [MAINACCT5]       NVARCHAR (20) NULL,
    [SOLVEORDER5]     NVARCHAR (20) NULL,
    [SEQ6]            NVARCHAR (50) NULL,
    [ID6]             NVARCHAR (20) NULL,
    [ACCTYPE6]        NVARCHAR (3)  NULL,
    [CALC6]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION6]  NVARCHAR (50) NULL,
    [HLEVEL6]         NVARCHAR (2)  NULL,
    [SCALING6]        SMALLINT      NULL,
    [RATETYPE6]       NVARCHAR (20) NULL,
    [FORMULA6]        VARCHAR (600) NULL,
    [MAINACCT6]       NVARCHAR (20) NULL,
    [SOLVEORDER6]     NVARCHAR (20) NULL,
    [SEQ7]            NVARCHAR (50) NULL,
    [ID7]             NVARCHAR (20) NULL,
    [ACCTYPE7]        NVARCHAR (3)  NULL,
    [CALC7]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION7]  NVARCHAR (50) NULL,
    [HLEVEL7]         NVARCHAR (2)  NULL,
    [SCALING7]        SMALLINT      NULL,
    [RATETYPE7]       NVARCHAR (20) NULL,
    [FORMULA7]        VARCHAR (600) NULL,
    [MAINACCT7]       NVARCHAR (20) NULL,
    [SOLVEORDER7]     NVARCHAR (20) NULL,
    [SEQ8]            NVARCHAR (50) NULL,
    [ID8]             NVARCHAR (20) NULL,
    [ACCTYPE8]        NVARCHAR (3)  NULL,
    [CALC8]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION8]  NVARCHAR (50) NULL,
    [HLEVEL8]         NVARCHAR (2)  NULL,
    [SCALING8]        SMALLINT      NULL,
    [RATETYPE8]       NVARCHAR (20) NULL,
    [FORMULA8]        VARCHAR (600) NULL,
    [MAINACCT8]       NVARCHAR (20) NULL,
    [SOLVEORDER8]     NVARCHAR (20) NULL,
    [SEQ9]            NVARCHAR (50) NULL,
    [ID9]             NVARCHAR (20) NULL,
    [ACCTYPE9]        NVARCHAR (3)  NULL,
    [CALC9]           NVARCHAR (1)  NULL,
    [EVDESCRIPTION9]  NVARCHAR (50) NULL,
    [HLEVEL9]         NVARCHAR (2)  NULL,
    [SCALING9]        SMALLINT      NULL,
    [RATETYPE9]       NVARCHAR (20) NULL,
    [FORMULA9]        VARCHAR (600) NULL,
    [MAINACCT9]       NVARCHAR (20) NULL,
    [SOLVEORDER9]     NVARCHAR (20) NULL,
    [SEQ10]           NVARCHAR (50) NULL,
    [ID10]            NVARCHAR (20) NULL,
    [ACCTYPE10]       NVARCHAR (3)  NULL,
    [CALC10]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION10] NVARCHAR (50) NULL,
    [HLEVEL10]        NVARCHAR (2)  NULL,
    [SCALING10]       SMALLINT      NULL,
    [RATETYPE10]      NVARCHAR (20) NULL,
    [FORMULA10]       VARCHAR (600) NULL,
    [MAINACCT10]      NVARCHAR (20) NULL,
    [SOLVEORDER10]    NVARCHAR (20) NULL,
    [ORGANIZATION]    NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimAccountPC]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimAccountPC]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimAccountPC]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimAccountPC]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimAccountPC]([ID5] ASC);


GO
CREATE NONCLUSTERED INDEX [ID6]
    ON [dbo].[dimAccountPC]([ID6] ASC);


GO
CREATE NONCLUSTERED INDEX [ID7]
    ON [dbo].[dimAccountPC]([ID7] ASC);


GO
CREATE NONCLUSTERED INDEX [ID8]
    ON [dbo].[dimAccountPC]([ID8] ASC);


GO
CREATE NONCLUSTERED INDEX [ID9]
    ON [dbo].[dimAccountPC]([ID9] ASC);


GO
CREATE NONCLUSTERED INDEX [ID10]
    ON [dbo].[dimAccountPC]([ID10] ASC);

