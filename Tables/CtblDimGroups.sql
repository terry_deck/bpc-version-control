﻿CREATE TABLE [dbo].[CtblDimGroups] (
    [AGREGAT]      NVARCHAR (20) NULL,
    [ID]           NVARCHAR (20) NULL,
    [Organization] NVARCHAR (3)  NULL,
    [Niv]          INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_AGREGAT]
    ON [dbo].[CtblDimGroups]([AGREGAT] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ID]
    ON [dbo].[CtblDimGroups]([ID] ASC);

