﻿CREATE TABLE [dbo].[acsCategory] (
    [APP]       NVARCHAR (30)  NOT NULL,
    [ProfileID] NVARCHAR (100) NOT NULL,
    [Member]    NVARCHAR (20)  NOT NULL,
    [RW]        NVARCHAR (1)   NULL
);


GO
CREATE CLUSTERED INDEX [CIDX_Category_01]
    ON [dbo].[acsCategory]([ProfileID] ASC, [APP] ASC, [Member] ASC);

