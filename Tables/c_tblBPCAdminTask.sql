﻿CREATE TABLE [dbo].[c_tblBPCAdminTask] (
    [Task_ID]     NUMERIC (18)   NOT NULL,
    [Task_Name]   NVARCHAR (100) NOT NULL,
    [Task_Desc]   NVARCHAR (300) NULL,
    [eff_dt]      DATETIME       NOT NULL,
    [exp_dt]      DATETIME       NULL,
    [last_chg_dt] DATETIME       NOT NULL,
    CONSTRAINT [PK_c_tblBPCAdminTask] PRIMARY KEY CLUSTERED ([Task_ID] ASC)
);

