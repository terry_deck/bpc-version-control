﻿CREATE proc [dbo].[csp_Aud_MainLogAdd] (
	  @main_name sysname
	, @feat_group varchar(50)
	, @feat_step smallint = 1
) AS

set nocount on

insert aud_main_log ( main_name, feat_group, feat_step)
	select @main_name, @feat_group, @feat_step

return @@IDENTITY

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_Aud_MainLogAdd]
		@main_name = 'Process Customer dimension',
		@feat_group = N'MetaAuto',
		@feat_step = 1

SELECT	'Return Value' = @return_value

*/

/****** Object:  StoredProcedure [dbo].[csp_Aud_MainLogUpd]    Script Date: 04/10/2008 23:41:20 ******/
SET ANSI_NULLS ON
