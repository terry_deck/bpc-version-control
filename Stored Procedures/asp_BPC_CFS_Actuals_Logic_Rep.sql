﻿

------------------------------asp_BPC_CFS_Actuals_Logic_Rep------------------------------------------------------
---THIS PROCEDURE IS USED in the creation of the Actuals Logic run-time report
---
--- 31/08/2011 T Deck
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_CFS_Actuals_Logic_Rep] @rpstart nvarchar(10), @rpend nvarchar(10)
AS
SET NOCOUNT ON;


BEGIN
DECLARE @rpstrt datetime;
DECLARE @rped datetime;

select @rpstrt = convert(datetime, @rpstart, 103);
select @rped = convert(datetime, @rpend, 103);


SELECT 
	 a.starttime AS 'RunDate',
	 DATEDIFF(MINUTE,a.StartTime,a.EndTime) AS 'Duration',
	 a.PackageID as 'Package' ,  
	 a.ownerid ,
	 b.fullname
	        
  
  FROM dbo.tblDTSLog a, dbo.tblUsers b
  where PackageID in ('Actual Default','Actual Consolidation','Default&Consol Pkg',
                      'Pull from Main','Pull from Main&Def&Con Pkg')
  and Appset in ('PROD_COBPC')
  and Status = 'Completed'
  and StartTime between @rpstrt and @rped
  and a.OwnerID = b.UserID
  order by StartTime
  
END
