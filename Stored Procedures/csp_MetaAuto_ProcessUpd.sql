﻿CREATE PROC [dbo].[csp_MetaAuto_ProcessUpd] (
	  @mods_tbl sysname 
	, @trg_tbl sysname 
	, @main_log_id int
) AS 

set nocount on

declare 
	  @tbl_nm sysname
	, @dim_nm sysname
	, @fld_nm sysname
	, @sql nvarchar(max)
	
set @sql = 'declare upd_cursor cursor fast_forward for 
	select distinct dim_nm, fld_nm from ' + @mods_tbl + '
	where mod_type = ''UPD'''

print @sql
exec(@sql)

open upd_cursor

-- get the first row
fetch next from upd_cursor
	into @dim_nm, @fld_nm

-- compare field for the certain level and write the chg record if it differs
while @@fetch_status = 0 begin

	set @sql = N'update d
		set d.' + @fld_nm + ' = mods.src_val
		from ' + @trg_tbl + ' d 
		inner join (select id, src_val from ' + @mods_tbl + ' 
				where dim_nm = ''' + @dim_nm + '''
				  and mod_type = ''UPD'' 
				  and fld_nm = ''' + @fld_nm + ''') mods
		on d.ID = mods.id'

	print @sql
	exec (@sql)

	-- get the next row
	fetch next from upd_cursor
		into @dim_nm, @fld_nm

end

close upd_cursor
deallocate upd_cursor

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_ProcessNew]    Script Date: 08/13/2008 01:17:57 ******/
SET ANSI_NULLS ON
