﻿
------------------------------asp_BPC_User_REPORT------------------------------------------------------
---THIS PROCEDURE IS USED in the creation of the user report
---It accepts a start and end profile id and lists details of the users with access to those pro
--- 31/08/2011 T Deck
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_User_REPORT]  @StartProfile nvarchar(100), @EndProfile nvarchar(100)
AS
SET NOCOUNT ON;


BEGIN
SELECT a.UserID, b.FullName,a.ProfileID,a.ProfileClass,a.Inheritance
  FROM [dbo].[UserProfile] a
  
  inner join [dbo].tblusers b
  on a.userid = b.userid
  where a.ProfileID between @StartProfile and @EndProfile
  
END