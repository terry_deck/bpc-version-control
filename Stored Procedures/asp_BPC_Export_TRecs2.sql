USE [PROD_COBPC]
GO
/****** Object:  StoredProcedure [dbo].[asp_BPC_Export_TRecs2]    Script Date: 22/03/2018 8:54:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------asp_BPC_Export_TRecs2------------------------------------------------
-- Created Oct 2018 for Trecs phase 2      T Deck
--
-- Updated Jan 2018 Required changes to deal with records which have a balance one day but
-- zero the next.  Zero balances are not allowed in BPC but it throws off Trintech's 
-- recociliation if records disappear so we create zero balance records for those.
--------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[asp_BPC_Export_TRecs2]

AS
SET NOCOUNT ON;

-------------- This table was created for changes Jan 2018   T. Deck   --------------------
BEGIN
IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aBPC_TRECS1')
CREATE TABLE dbo.aBPC_TRECS1(--CREATES TABLE IF IT DOESN'T EXIST
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[PERIOD]	  NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,2),
[ACCTYPE]    NVARCHAR(20))

ELSE
TRUNCATE TABLE DBO.aBPC_TRECS1
END

-------------- This table was created for changes Jan 2018   T. Deck   --------------------
-------------- It contains a backup of previous day info in case of    --------------------
-------------- issues with the package. We can restore PREVDAY from    --------------------
-------------- this backup if needed                                   --------------------
BEGIN
IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aBPC_TRECS_PREVDAY_BKUP')
CREATE TABLE dbo.aBPC_TRECS_PREVDAY_BKUP(--CREATES TABLE IF IT DOESN'T EXIST
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[PERIOD]	  NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,2),
[ACCTYPE]    NVARCHAR(20))
ELSE
   BEGIN
      TRUNCATE TABLE DBO.aBPC_TRECS_PREVDAY_BKUP
      INSERT INTO DBO.aBPC_TRECS_PREVDAY_BKUP ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],
	              [PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
				  [PERIOD],[SIGNEDDATA], [ACCTYPE])
      SELECT COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,PRODUCT,LOB,PAR,PROJECT,TREATY,
                REINSURER,LOSSYEAR,PERIOD,SIGNEDDATA,ACCTYPE
      FROM DBO.aBPC_TRECS_PREVDAY
   END
END
-------------- This table was created for changes Jan 2018   T. Deck   --------------------
BEGIN
IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aBPC_TRECS_PREVDAY')
CREATE TABLE dbo.aBPC_TRECS_PREVDAY(--CREATES TABLE IF IT DOESN'T EXIST
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[PERIOD]	  NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,2),
[ACCTYPE]    NVARCHAR(20))
ELSE
   BEGIN
      TRUNCATE TABLE DBO.aBPC_TRECS_PREVDAY
-------------- If the date isn't the 1st of the month copy the data from TRECS2 --------
-------------- into PREVDAY otherwise leave it without data                     --------
      IF DATEPART(DAY, GETDATE()) <> 1
	     INSERT INTO DBO.aBPC_TRECS_PREVDAY ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
         SELECT COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,PRODUCT,LOB,PAR,PROJECT,TREATY,
                REINSURER,LOSSYEAR,PERIOD,SIGNEDDATA,ACCTYPE
         FROM DBO.aBPC_TRECS2
   END
END

BEGIN
IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aBPC_TRECS2')


CREATE TABLE dbo.aBPC_TRECS2(--CREATES TABLE IF IT DOESN'T EXIST
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[PERIOD]	  NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,2),
[ACCTYPE]    NVARCHAR(20))

ELSE
TRUNCATE TABLE DBO.aBPC_TRECS2
END



BEGIN

	
	DECLARE @searchid TABLE (list varchar(500))
	DECLARE @period AS NVARCHAR(6)
	
	----- this defines the CLIC (company 060) accounts we want summed by LOB
	DECLARE @clicaccts TABLE (list varchar(2000))
	INSERT INTO @clicaccts
	VALUES ('113000_ACC'),('123330_ACC'),('125030_ACC'),('126000_ACC'),('203090_ACC'),
	       ('204000_ACC'),('204020_ACC'),('210010_ACC'),('228142_ACC'),('228144_ACC'),
		   ('228146_ACC'),('228164_ACC'),('228166_ACC'),('228210_ACC'),('228620_ACC'),
		   ('262000_ACC'),('401002_ACC'),('401005_ACC'),('401008_ACC'),('401010_ACC'),
		   ('401030_ACC'),('401032_ACC'),('401200_ACC'),('401320_ACC'),('502005_ACC'),
		   ('502015_ACC'),('502040_ACC'),('502205_ACC'),('502302_ACC'),('502304_ACC'),
		   ('505000_ACC'),('507020_ACC'),('508000_ACC'),('606000_ACC'),('606002_ACC'),
		   ('606006_ACC'),('606150_ACC'),('706090_ACC'),('706305_ACC'),('706600_ACC')

	----- this defines the CUMIS LIFE (company 090) accounts we want summed by LOB
	DECLARE @cumisaccts TABLE (list varchar(2000))
	INSERT INTO @cumisaccts
	VALUES ('203000_ACC'),('204000_ACC'),('210010_ACC'),('210640_ACC'),('228142_ACC'),
	       ('228144_ACC'),('228620_ACC'),('401005_ACC'),('401032_ACC'),('401200_ACC'),
		   ('401315_ACC'),('502005_ACC'),('502205_ACC'),('502302_ACC'),('505000_ACC'),
		   ('606000_ACC'),('606002_ACC'),('606006_ACC'),('606150_ACC'),('701200_ACC'),
		   ('701210_ACC'),('702007_ACC'),('702103_ACC'),('702212_ACC'),('702308_ACC'),
		   ('702318_ACC'),('702320_ACC'),('704000_ACC'),('704100_ACC'),('704200_ACC'),
		   ('704300_ACC'),('704305_ACC'),('704310_ACC'),('704700_ACC'),('706000_ACC'),
		   ('706001_ACC'),('706265_ACC')
		   ----- this defines the CGIC (company 021) accounts we want summed by Province
	DECLARE @cgicaccts TABLE (list varchar(2000))
	INSERT INTO @cgicaccts
	VALUES ('228016_ACC')
	
SET @period = (select substring(max(period),6,3)+'-'+substring(max(period),3,2) from dbo.aBPC_YTDTRECS2)
	

------- This section is for company not 060 or 090 or 21 -------------------------------
INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype   from
(select company, account, signeddata from dbo.aBPC_YTDTRECS2
where company in ('001_COM','003_COM','014_COM','023_COM','024_COM',
				  '029_COM','040_COM','045_COM','048_COM','054_COM',
				  '055_COM','056_COM','057_COM','059_COM','091_COM',
				  '092_COM','093_COM','220_COM','221_COM','222_COM',
				  '224_COM','0TQ_COM','0TR_COM','111_COM','200_COM')) t, dbo.mbraccount a
where a.id = t.account
group by COMPANY, ACCOUNT, ACCTYPE
order by company, account

--------------This section is for company 060 where account is not in the ----
--------------list of accounts we want summarized by company account lob  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, signeddata from dbo.aBPC_YTDTRECS2
where company = '060_COM'
and account not in (select list from @clicaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, ACCTYPE
order by company, account

--------------This section is for company 060 where account is in the --------
--------------list of accounts we want summarized by company account lob  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', substring(lob,1,3), 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, lob, signeddata from dbo.aBPC_YTDTRECS2
where company = '060_COM'
and account in (select list from @clicaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, lob, ACCTYPE
order by company, account, lob

--------------This section is for company 090 where account is not in the ----
--------------list of accounts we want summarized by company account lob  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, signeddata from dbo.aBPC_YTDTRECS2
where company = '090_COM'
and account not in (select list from @cumisaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, ACCTYPE
order by company, account

--------------This section is for company 090 where account is in the --------
--------------list of accounts we want summarized by company account lob  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', substring(lob,1,3), 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, lob, signeddata from dbo.aBPC_YTDTRECS2
where company = '090_COM'
and account in (select list from @cumisaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, lob, ACCTYPE
order by company, account, lob

--------------This section is for company 021 where account is not in the ----
--------------list of accounts we want summarized by company account province  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, signeddata from dbo.aBPC_YTDTRECS2
where company = '021_COM'
and account not in (select list from @cgicaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, ACCTYPE
order by company, account

--------------This section is for company 021 where account is in the --------
--------------list of accounts we want summarized by company account province  ------------

INSERT INTO DBO.aBPC_TRECS1 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
select substring(company,1,3), 'T', substring(account,1,6), 'T', substring(province,1,2), 'T', 'T', 'T', 'T', 'T', 'T', 'T',
'T', @period, round(sum(t.signeddata),2), a.acctype  from
(select company, account, province, signeddata from dbo.aBPC_YTDTRECS2
where company = '021_COM'
and account in (select list from @cgicaccts )) t, dbo.mbraccount a
where a.id = t.account
--and SUBSTRING(account,1,1) in ('1','2','3','4','5','6','7','8','9')) t
group by COMPANY, ACCOUNT, province, ACCTYPE
order by company, account, province
END

-------------- The following 2 sections of code were added Jan 2018 to deal ---------------
-------------- with records which have a value one day then are zero value  ---------------
-------------- the next.  We search for records which existed yesterday     ---------------
-------------- but don't exist in todays data.  We then write zero amount   ---------------
-------------- records for those values as Trintech cannot deal with recs   ---------------
-------------- which go missing.  T.Deck Jan 2018                           ---------------
BEGIN
   INSERT INTO DBO.aBPC_TRECS2 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])
   SELECT COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,PRODUCT,LOB,PAR,PROJECT,TREATY,
          REINSURER,LOSSYEAR,PERIOD,SIGNEDDATA,ACCTYPE
   FROM DBO.aBPC_TRECS1
END

BEGIN
   INSERT INTO DBO.aBPC_TRECS2 ([COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],
							[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],
							[PERIOD],[SIGNEDDATA], [ACCTYPE])  
   SELECT COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,PRODUCT,LOB,PAR,PROJECT,TREATY,
          REINSURER,LOSSYEAR,PERIOD,0,ACCTYPE
   FROM  DBO.aBPC_TRECS_PREVDAY TP
   WHERE NOT EXISTS (SELECT * from DBO.aBPC_TRECS1 T1 
                     WHERE T1.COMPANY+T1.COSTCENTRE+T1.ACCOUNT+T1.INTCO+T1.PROVINCE+
					       T1.PROGRAM+T1.PRODUCT+T1.LOB+T1.PAR+T1.PROJECT+T1.TREATY+
						   T1.REINSURER+T1.LOSSYEAR+T1.PERIOD = TP.COMPANY+TP.COSTCENTRE+
						   TP.ACCOUNT+TP.INTCO+TP.PROVINCE+TP.PROGRAM+TP.PRODUCT+TP.LOB+
						   TP.PAR+TP.PROJECT+TP.TREATY+TP.REINSURER+TP.LOSSYEAR+TP.PERIOD)
END


