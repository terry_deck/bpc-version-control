﻿
CREATE procedure [dbo].[A_BAD_COMPANY] @company nvarchar(1000)
 
as	

/*  This procedure is designed to determine if more than one company member is being passed into logic 
and if so, cause logic to fail and stop running.  The procedure searches for a comma in the
time string passed to the procedure via logic.  If a comma exists that means there is more
than one member being passed and it fails by inserting data into a non-existent table.
Copyright 2010 Column5 Consulting
Created by Mike Dunn
Contact:  mdunn@column5.com
*/

DECLARE @List nvarchar(4000)	-- variable to store the list passed to the procedure
DECLARE @ListItem nvarchar(20)  -- variable to hold the parsed value
DECLARE @Pos int				-- variable to store the position value of the comma

-- assign list to the company string passed in
SET @List = @company
       -- Get the position of the first comma (returns 0 if no commas left in string)
	SET @Pos = CHARINDEX(',', @List)

      -- Extract the list item string
	IF @Pos = 0
       Begin
               SET @ListItem = @List
       End
       ELSE
       Begin	
				-- This statement causes the procedure and logic to fail becuase the table does not exist
               Insert into PLEASE_ONLY_RUN_FOR_ONE_COMPANY select 'COMPANY', @ListItem
       End
