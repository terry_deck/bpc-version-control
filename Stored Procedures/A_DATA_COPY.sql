﻿






CREATE procedure [dbo].[A_DATA_COPY] @bpccategory nvarchar(20), @bpctime nvarchar(1000)

as	

/*

A_DATA_COPY Stored Procedure

This procedure is designed to copy data from one application to another.  It uses temp tables and optimizes the result
prior to loading.  It skips the following dimensions:

COSTCENTRE
PROGRAM
PROJECT

It adds the following dimensions with their default members:

FLOW - F_CLOSED
GROUPS - 000_GRP
RPTCURRENCY - CAD

It also remaps the TREATY and REINSURER dimensions to the TREATYC and REINSURERC dimensions.

The BPC Script Logic to call this procedure looks as follows:

*RUN_STORED_PROCEDURE=A_DATA_COPY('%CATEGORY_SET%','%TIME_SET%')

Copyright 2013 Column5 Consulting
Created by Stefan Dunhem
Contact:  sdunhem@column5.com

*/
---- Last updated by T Deck 
---- Added 17-09-2015 for pooling project. Actual_pool category to write to specific members


----------------------------------------------------------------------------------------
------------------------CREATE TEMP TABLES----------------------------------------------
----------------------------------------------------------------------------------------

-- Create a temp table and load variables passed via stored proc into a scope table.
-- For use in filtering the SQL statement later on.  This allows us to accept multiple comma delimited members.

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_SCOPE1')
	DROP TABLE [ASP_COPY_SCOPE1]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_SCOPE2')
	DROP TABLE [ASP_COPY_SCOPE2]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_OMIT')
	DROP TABLE [ASP_COPY_OMIT]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_MAPPING_T')
	DROP TABLE [ASP_COPY_MAPPING_T]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_MAPPING_R')
	DROP TABLE [ASP_COPY_MAPPING_R]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_DATA1')
	DROP TABLE [ASP_COPY_DATA1]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_DATA2')
	DROP TABLE [ASP_COPY_DATA2]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_DATA3')
	DROP TABLE [ASP_COPY_DATA3]

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASP_COPY_DATA4')
	DROP TABLE [ASP_COPY_DATA4]

create table ASP_COPY_SCOPE1 (
DIMENSION nvarchar(20),
MEMBER NVARCHAR(20) )

create table ASP_COPY_SCOPE2 (
DIMENSION NVARCHAR(20),
MEMBER NVARCHAR(20) )

create table ASP_COPY_OMIT (
DIMENSION NVARCHAR(20),
MEMBER NVARCHAR(20) )

create table ASP_COPY_MAPPING_T (
TREATY NVARCHAR(20),
DESTINATIONTREATY NVARCHAR(20) )

create table ASP_COPY_MAPPING_R (
REINSURER NVARCHAR(20),
DESTINATIONREINSURER NVARCHAR(20) )

create table ASP_COPY_DATA1 (
CATEGORY NVARCHAR(20),
COMPANY NVARCHAR(20),
DATASRC NVARCHAR(20),
INTCO NVARCHAR(20),
FLOW NVARCHAR(20),
GROUPS NVARCHAR(20),
RPTCURRENCY NVARCHAR(20),
LOB NVARCHAR(20),
LOSSYEAR NVARCHAR(20),
PAR NVARCHAR(20),
PRODUCT NVARCHAR(20),
PROVINCE NVARCHAR(20),
REINSURERC NVARCHAR(20),
ACCOUNT NVARCHAR(20),
SIGNEDDATA DECIMAL(25,10),
SOURCE TINYINT,
TIMEID NVARCHAR(20),
TREATYC NVARCHAR(20) )

create table ASP_COPY_DATA2 (
CATEGORY NVARCHAR(20),
COMPANY NVARCHAR(20),
DATASRC NVARCHAR(20),
INTCO NVARCHAR(20),
FLOW NVARCHAR(20),
GROUPS NVARCHAR(20),
RPTCURRENCY NVARCHAR(20),
LOB NVARCHAR(20),
LOSSYEAR NVARCHAR(20),
PAR NVARCHAR(20),
PRODUCT NVARCHAR(20),
PROVINCE NVARCHAR(20),
REINSURERC NVARCHAR(20),
ACCOUNT NVARCHAR(20),
SIGNEDDATA DECIMAL(25,10),
SOURCE TINYINT,
TIMEID NVARCHAR(20),
TREATYC NVARCHAR(20) )

create table ASP_COPY_DATA3 (
CATEGORY NVARCHAR(20),
COMPANY NVARCHAR(20),
DATASRC NVARCHAR(20),
INTCO NVARCHAR(20),
FLOW NVARCHAR(20),
GROUPS NVARCHAR(20),
RPTCURRENCY NVARCHAR(20),
LOB NVARCHAR(20),
LOSSYEAR NVARCHAR(20),
PAR NVARCHAR(20),
PRODUCT NVARCHAR(20),
PROVINCE NVARCHAR(20),
REINSURERC NVARCHAR(20),
ACCOUNT NVARCHAR(20),
SIGNEDDATA DECIMAL(25,10),
SOURCE TINYINT,
TIMEID NVARCHAR(20),
TREATYC NVARCHAR(20) )

create table ASP_COPY_DATA4 (
CATEGORY NVARCHAR(20),
COMPANY NVARCHAR(20),
DATASRC NVARCHAR(20),
INTCO NVARCHAR(20),
FLOW NVARCHAR(20),
GROUPS NVARCHAR(20),
RPTCURRENCY NVARCHAR(20),
LOB NVARCHAR(20),
LOSSYEAR NVARCHAR(20),
PAR NVARCHAR(20),
PRODUCT NVARCHAR(20),
PROVINCE NVARCHAR(20),
REINSURERC NVARCHAR(20),
ACCOUNT NVARCHAR(20),
SIGNEDDATA DECIMAL(25,10),
SOURCE TINYINT,
TIMEID NVARCHAR(20),
TREATYC NVARCHAR(20) )

----------------------------------------------------------------------------------------
------------------------PARSE OUT COMMA-SEPARATED CATEGORY MEMBERS----------------------
----------------------------------------------------------------------------------------

DECLARE @List nvarchar(1000) -- variable to hold the list of comma delimited BPC time members
DECLARE @ListItem nvarchar(20) -- variable to hold the parsed member (time)
DECLARE @Pos int  -- variable to hold the position value of the first comma in the list

-- Assign category string passed to procedure to the List variable.
SET @List = @bpccategory

-- Loop while the list string still holds one or more characters.
WHILE LEN(@List) > 0
Begin
       -- Get the position of the first comma (returns 0 if no commas left in string).
       SET @Pos = CHARINDEX(',', @List)

       -- Extract the list item string.
       IF @Pos = 0
       Begin
               SET @ListItem = @List
       End
       ELSE
       Begin
               SET @ListItem = SUBSTRING(@List, 1, @Pos - 1)
       End

		-- Insert the parsed category into the scope table.
	   Insert into ASP_COPY_SCOPE1 select 'CATEGORY', @ListItem
	   Insert into ASP_COPY_SCOPE2 select 'CATEGORY', @ListItem

       -- Remove the list item (and trailing comma if present) from the list string.
       IF @Pos = 0
       Begin
               SET @List = ''
       End
       ELSE
       Begin
               -- Start substring at the character after the first comma.
                SET @List = SUBSTRING(@List, @Pos + 1, LEN(@List) - @Pos)
       End
End

----------------------------------------------------------------------------------------
------------------------PARSE OUT COMMA-SEPARATED TIME MEMBERS--------------------------
----------------------------------------------------------------------------------------

-- Assign time string passed to procedure to the List variable.
SET @List = @bpctime

-- Loop while the list string still holds one or more characters.
WHILE LEN(@List) > 0
Begin
       -- Get the position of the first comma (returns 0 if no commas left in string).
       SET @Pos = CHARINDEX(',', @List)

       -- Extract the list item string.
       IF @Pos = 0
       Begin
               SET @ListItem = @List
       End
       ELSE
       Begin
               SET @ListItem = SUBSTRING(@List, 1, @Pos - 1)
       End

		-- Insert the parsed BPC time member into the scope table.
	   Insert into ASP_COPY_SCOPE1 select 'TIME', @ListItem

       -- Remove the list item (and trailing comma if present) from the list string.
       IF @Pos = 0
       Begin
               SET @List = ''
       End
       ELSE
       Begin
               -- Start substring at the character after the first comma.
                SET @List = SUBSTRING(@List, @Pos + 1, LEN(@List) - @Pos)
       End
End

----------------------------------------------------------------------------------------
------------------------REMAP FORMAT OF TIME ID'S---------------------------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_SCOPE2 (DIMENSION,MEMBER)
select 'TIME' as DIMENSION,TIMEID
from dbo.dimTime
where TIMEIDNAME1 in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'TIME')

----------------------------------------------------------------------------------------
------------------------CREATE LIST OF COMPANIES TO OMIT--------------------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_OMIT (DIMENSION,MEMBER)
select 'COMPANY' as DIMENSION,ID
from dbo.mbrCompany
where PARENTH1 in ('S17_COM','SA9_COM') and CALC='N' and ISBASEMEM='Y'

insert into ASP_COPY_OMIT (DIMENSION,MEMBER)
select 'COMPANY' as DIMENSION,ID
from dbo.mbrCompany
where ID in ('068_COM','078_COM')

----------------------------------------------------------------------------------------
------------------------CREATE MAPPING TABLES-------------------------------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_MAPPING_T (TREATY,DESTINATIONTREATY)
select ID,CONSOLTREATY from dbo.mbrTreaty
where CALC='N'
and ISBASEMEM='Y'
and CONSOLTREATY<>''

insert into ASP_COPY_MAPPING_R (REINSURER,DESTINATIONREINSURER)
select ID,CONSOLREINSURER from dbo.mbrReinsurer
where CALC='N'
and ISBASEMEM='Y'
and CONSOLREINSURER<>''

----------------------------------------------------------------------------------------
------------------------COPY SOURCE DATA INTO DATA TABLE 1------------------------------
----------------------------------------------------------------------------------------
---- Added 17-09-2015 for pooling project. Actual_pool category to write to specific members

insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD',LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFACTWBMain
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY <> 'ACTUAL_POOL'

insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD',LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFAC2Main
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY <> 'ACTUAL_POOL'

insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD',LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFACTMain
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY <> 'ACTUAL_POOL'


insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD','000_LOB','0000_LYR',PAR,'00_PRD','00_PRV',REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFACTWBMain
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY = 'ACTUAL_POOL'

insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD','000_LOB','0000_LYR',PAR,'00_PRD','00_PRV',REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFAC2Main
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY = 'ACTUAL_POOL'


insert into ASP_COPY_DATA1 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,'F_CLOSED','000_GRP','CAD','000_LOB','0000_LYR',PAR,'00_PRD','00_PRV',REINSURER,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATY
from dbo.tblFACTMain
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE1 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')
and CATEGORY = 'ACTUAL_POOL'


----------------------------------------------------------------------------------------
------------------------OMIT CERTAIN COMPANIES------------------------------------------
----------------------------------------------------------------------------------------

delete from ASP_COPY_DATA1
where COMPANY in (select distinct MEMBER from ASP_COPY_OMIT where DIMENSION = 'COMPANY')

----------------------------------------------------------------------------------------
------------------------REMAP TREATY AND POST INTO DATA TABLE 2-------------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_DATA2 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select A.CATEGORY,A.DATASRC,A.COMPANY,A.INTCO,A.FLOW,A.GROUPS,A.RPTCURRENCY,A.LOB,A.LOSSYEAR,A.PAR,A.PRODUCT,A.PROVINCE,A.REINSURERC,A.ACCOUNT,A.SIGNEDDATA,A.SOURCE,A.TIMEID,B.DESTINATIONTREATY
from ASP_COPY_DATA1 A
join ASP_COPY_MAPPING_T B on A.TREATYC=B.TREATY

----------------------------------------------------------------------------------------
------------------------REMAP REINSURER AND POST INTO DATA TABLE 3----------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_DATA3 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select A.CATEGORY,A.DATASRC,A.COMPANY,A.INTCO,A.FLOW,A.GROUPS,A.RPTCURRENCY,A.LOB,A.LOSSYEAR,A.PAR,A.PRODUCT,A.PROVINCE,B.DESTINATIONREINSURER,A.ACCOUNT,A.SIGNEDDATA,A.SOURCE,A.TIMEID,A.TREATYC
from ASP_COPY_DATA2 A
join ASP_COPY_MAPPING_R B on A.REINSURERC=B.REINSURER

----------------------------------------------------------------------------------------
------------------------COMPRESS SIGNEDDATA AND POST INTO DATA TABLE 4------------------
----------------------------------------------------------------------------------------

insert into ASP_COPY_DATA4 (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SUM(SIGNEDDATA) as SIGNEDDATA,SOURCE,TIMEID,TREATYC
from ASP_COPY_DATA3
group by CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SOURCE,TIMEID,TREATYC

----------------------------------------------------------------------------------------
------------------------DELETE DESTINATION RECORDS--------------------------------------
----------------------------------------------------------------------------------------

delete from dbo.tblFACTWBConsol
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')

delete from dbo.tblFAC2Consol
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')

delete from dbo.tblFACTConsol
WHERE CATEGORY in (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'CATEGORY') and TIMEID IN (select distinct MEMBER from ASP_COPY_SCOPE2 where DIMENSION = 'TIME')
and DATASRC in ('ORACLE','POOLING','POOLING_OPEN','POOLING_JRN','POOLING_CFD')

----------------------------------------------------------------------------------------
------------------------COPY FINAL RESULT INTO FACT TABLE-------------------------------
----------------------------------------------------------------------------------------

insert into dbo.tblFACTConsol (CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC)
select CATEGORY,DATASRC,COMPANY,INTCO,FLOW,GROUPS,RPTCURRENCY,LOB,LOSSYEAR,PAR,PRODUCT,PROVINCE,REINSURERC,ACCOUNT,SIGNEDDATA,SOURCE,TIMEID,TREATYC
from ASP_COPY_DATA4

----------------------------------------------------------------------------------------
------------------------END OF STORED PROCEDURE-----------------------------------------
----------------------------------------------------------------------------------------

