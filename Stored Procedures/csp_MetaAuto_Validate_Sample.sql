﻿CREATE PROC [dbo].[csp_MetaAuto_Validate_Sample] (
	  @dim_nm nvarchar(50) = 'Customer'
	, @tmp_tbl sysname = 'metaTmpCustomer'
	, @src_tbl sysname = 'metaSrcCustomer'
	, @trg_tbl sysname = 'mbrCustomer'
) as

-- this is just a sample approach to validating the structure

-- validation
-- the source table exists
-- the target table exists
-- the temp table exists
-- the source fields exist
-- the target fields exist
-- the upd_where clauses are valid (include source.x and target.x)


-- the source table exists
if object_id(@src_tbl) is null 
	print 'Source recordset table [' + @src_tbl + '] does not exist'

-- the target table exists
if object_id(@trg_tbl) is null 
	print 'Target dim table [' + @trg_tbl + '] does not exist'

-- the temp table exists
if object_id(@tmp_tbl) is null 
	print 'Temp dim table [' + @tmp_tbl + ']e does not exist'

-- the source fields exist
select @src_tbl tbl_name, d.dim_nm, d.src_fld 
	from (select distinct dim_nm, src_fld from mbrzzzMetaMapping) d
	left outer join information_schema.columns c
	  on d.src_fld = c.column_name
	 and @src_tbl = c.table_name 
	where c.column_name is null

-- the target fields exist
select @trg_tbl tbl_name, d.dim_nm, d.trg_fld 
	from (select distinct dim_nm, trg_fld from mbrzzzMetaMapping) d
	left outer join information_schema.columns c
	  on d.trg_fld = c.column_name
	 and @trg_tbl = c.table_name 
	where c.column_name is null

-- the temp fields exist
select @tmp_tbl tbl_name, d.dim_nm, d.trg_fld 
	from (select distinct dim_nm, trg_fld from mbrzzzMetaMapping) d
	left outer join information_schema.columns c
	  on d.trg_fld = c.column_name
	 and @tmp_tbl = c.table_name 
	where c.column_name is null

-- the upd_where clauses are valid (include source.x and target.x)
-- sSc: I don't know how to do this.

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_ProcessUpd]    Script Date: 08/13/2008 01:18:01 ******/
SET ANSI_NULLS ON
