﻿
CREATE procedure [dbo].[A_SAL_CCT_CHECK] @IncomingProp nvarchar(1)

as

/*
SAL_CCT Check Stored Procedure
This stored procedure checks to make sure at least one cost centre exists with the stated SAL_CCT property.
This is done to make sure allocations do not run on empty sets of members  (result would be executing on ALL cost centres).
If no cost centres are found the procedure will attempt to insert data into a non-existent table.
As a result, the procedure will fail and logic will stop running.
This prevents submissions of data to invalid intersections.

Copyright 2011 Column5 Consulting
Created by Stefan Dunhem
Contact:  sdunhem@column5.com
*/

DECLARE @MyProperty nvarchar(1) -- Variable to hold the property value passed in by logic.

SET @MyProperty = @IncomingProp

--If no cost centres are found...

IF (SELECT COUNT (*) FROM dbo.mbrCostCentre WHERE SAL_CCT=@MyProperty AND CALC='N')=0

--the below statement causes the procedure and logic to fail becuase the table does not exist

INSERT INTO PLEASE_CHECK_SAL_CCT_PROPERTY_AND_LOGIC SELECT * FROM dbo.mbrCostCentre

ELSE

--otherwise we just count the members in the Category member table.

SELECT COUNT (*) FROM dbo.mbrCategory
