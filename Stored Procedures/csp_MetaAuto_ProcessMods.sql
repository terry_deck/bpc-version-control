﻿CREATE PROC [dbo].[csp_MetaAuto_ProcessMods] (
	  @control_id nvarchar(50)  -- from zzzMetaControl
	, @main_log_id int = 1 -- the main audit log id
) as
--csp_MetaAuto_ProcessMods 'HRPosition1'
set nocount on 

declare 
	  @return_value int
	, @mods_tbl sysname
	, @tmp_tbl sysname
	, @trg_tbl sysname
	, @dim_nm sysname
	, @del_or_flag nvarchar(1)
	, @del_prop sysname
	, @del_prop_val nvarchar(255)
	, @proc_log_id int

EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_ProcessMods
	, @action_item = N'get control info'

-- get control table vars
PRINT 'GETTING CONTROL TABLE VARIABLES'
EXEC	@return_value = [dbo].[csp_MetaAuto_GetControlInfo]
		@control_id = @control_id,
		@src_tbl = NULL ,
		@tmp_tbl = @tmp_tbl OUTPUT,
		@trg_tbl = @trg_tbl OUTPUT,
		@dim_nm = @dim_nm OUTPUT,
		@del_or_flag = @del_or_flag OUTPUT,
		@del_prop = @del_prop OUTPUT,
		@del_prop_val = @del_prop_val OUTPUT

-- get mods table name
PRINT 'GETTING MODS TABLE NAME'
select @mods_tbl = 'metaMods_' + convert(varchar,@main_log_id)

EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

-- process updates
PRINT 'PROCESSING UPDATES'
if (select isnull(PROCESS_UPD,'') FROM mbrzzzMetaControl where ID = @control_id) in ('Y', '1')
begin

	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_ProcessMods
		, @action_item = N'update dimension properties'

	-- process updates
	EXEC	@return_value = [dbo].[csp_MetaAuto_ProcessUpd]
			@mods_tbl = @mods_tbl,
			@trg_tbl = @trg_tbl,
			@main_log_id = @main_log_id

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end

PRINT 'PROCESSING INSERTS'
-- process inserts
if (select isnull(PROCESS_INS,'') FROM mbrzzzMetaControl where ID = @control_id) in ('Y', '1')
begin

	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_ProcessMods
		, @action_item = N'insert new members'

	-- process inserts
	EXEC	@return_value = [dbo].[csp_MetaAuto_ProcessNew]
			@mods_tbl = @mods_tbl,
			@tmp_tbl = @tmp_tbl,
			@trg_tbl = @trg_tbl,
			@dim_nm = @dim_nm,
			@main_log_id = @main_log_id

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end

-- process deletes
PRINT 'PROCESSING DELETES'
if (select isnull(PROCESS_DEL,'') FROM mbrzzzMetaControl where ID = @control_id) in ('Y', '1')
begin

	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_ProcessMods
		, @action_item = N'delete or flag deleted members'

	-- process deletes
	EXEC	@return_value = [dbo].[csp_MetaAuto_ProcessDel]
			@mods_tbl = @mods_tbl,
			@trg_tbl = @trg_tbl,
			@dim_nm = @dim_nm,
			@main_log_id = @main_log_id,
			@delete_or_flag = @del_or_flag,
			@del_prop = @del_prop,
			@del_prop_val = @del_prop_val

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_MakeModsTbl]    Script Date: 08/13/2008 01:17:43 ******/
SET ANSI_NULLS ON
