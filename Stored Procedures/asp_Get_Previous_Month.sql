﻿




CREATE PROCEDURE [dbo].[asp_Get_Previous_Month]
@output nvarchar(6) OUT

AS
SET NOCOUNT ON;

BEGIN
	DECLARE @preMonth AS nvarchar(8)
	DECLARE @currMonth AS nvarchar(8)
	DECLARE @year AS NUMERIC
	DECLARE @mth NUMERIC

    SELECT @currMonth = CONVERT(VARCHAR(6), GETDATE(), 112)

	SELECT @preMonth = SUBSTRING(@currMonth, 5, 2)

	IF @preMonth = '01' 
	   SELECT @preMonth = CONVERT(nvarchar, CAST(SUBSTRING(@currMonth, 1, 4) AS INT)-1) + '12'
	ELSE IF @preMonth = '12' OR @preMonth = '11'
	   SELECT @preMonth = SUBSTRING(@currMonth, 1, 4) + CONVERT(nvarchar, CAST(SUBSTRING(@currMonth, 5, 2) AS INT)-1)
	ELSE
	   SELECT @preMonth = SUBSTRING(@currMonth, 1, 4) + '0'+ CONVERT(nvarchar, CAST(SUBSTRING(@currMonth, 5, 2) AS INT)-1)

	SELECT @output = @preMonth
END   




