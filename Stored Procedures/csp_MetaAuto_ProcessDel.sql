﻿CREATE PROC [dbo].[csp_MetaAuto_ProcessDel] (
	  @mods_tbl sysname
	, @trg_tbl sysname 
	, @dim_nm nvarchar(125)
	, @main_log_id int
	, @delete_or_flag nvarchar(1)
	, @del_prop sysname 
	, @del_prop_val nvarchar(200) = '%TIME% | Facts %FACTS%'
) AS 

set nocount on

declare 
	  @sql nvarchar(max)
	, @fld_list nvarchar(max)
	, @fld sysname
	, @cube_nm sysname
	, @del_prop_rendered nvarchar(max)
	, @del_id nvarchar(20)

-- validate parms

-- if the option is to do an ACTUAL DELETE then delete dimension members for which no fact data exists
--    if the member is a parent of anyone else then clear the parentHx column(s)
-- if the option is to flag deletes then update the property specified with the value specified

if @delete_or_flag = N'D' or charindex('%FACTS%', @del_prop_val) <> 0
begin

	-- build a query to update the fact count of the autometa_mods_X table deletion member(s)
	set @sql = N'
		update mods
		set dst_val = convert(nvarchar, facts.cnt), src_val = ''' + @delete_or_flag + '''
		from ' + @mods_tbl + ' mods 
		inner join (select ' + @dim_nm + ', count(*) cnt from ('

	-- get a cursor for all the applications this dimension belongs to
	declare app_cursor cursor fast_forward for
		select CubeName from tblDimension
		where DimName = @dim_nm
		  and isnull(CubeName,'') <> ''

	open app_cursor

	fetch next from app_cursor into @cube_nm
	while @@fetch_status = 0
	begin

		set @sql = @sql + N'
			select ' + @dim_nm + ' from tblFact' + @cube_nm + ' f inner join ' + @mods_tbl + ' mods
			  on mods.dim_nm = ''' + @dim_nm + '''
			 and mods.mod_type = ''DEL''
			 and mods.id = f.' + @dim_nm + '

			union all 	
			select ' + @dim_nm + ' from tblFac2' + @cube_nm + ' f inner join ' + @mods_tbl + ' mods
			  on mods.dim_nm = ''' + @dim_nm + '''
			 and mods.mod_type = ''DEL''
			 and mods.id = f.' + @dim_nm + '

			union all
			select ' + @dim_nm + ' from tblFactWB' + @cube_nm + ' f inner join ' + @mods_tbl + ' mods
			  on mods.dim_nm = ''' + @dim_nm + '''
			 and mods.mod_type = ''DEL''
			 and mods.id = f.' + @dim_nm 

		fetch next from app_cursor into @cube_nm

		if @@fetch_status = 0
			set @sql = @sql + N'
			union all'

	end

	set @sql = @sql + N'
				) f 
				group by ' + @dim_nm + ') facts
			on mods.dim_nm = ''' + @dim_nm + ''' 
			and mods.mod_type = ''DEL''
			and mods.id = facts.' + @dim_nm


	close app_cursor
	deallocate app_cursor

	-- execute fact finding query
	print @sql
	exec (@sql)


end

if @delete_or_flag = N'D'
begin

	-- delete members where there is no fact data
	set @sql = 'delete mbr from ' + @trg_tbl + ' mbr inner join ' + @mods_tbl + ' mods
	on mbr.ID = mods.ID
	where mods.mod_type = ''DEL''
	  and mods.dim_nm  = ''' + @dim_nm + '''
	  and mods.dst_val is null'

	print @sql
	exec (@sql)

end else -- @delete_or_flag <> N'D'
begin

	-- double the quotes
	set @del_prop_val = replace(@del_prop_val, '''', '''''') -- char(39), char(39) + char(39))

	set @sql = N'update mbr
		set ' + @del_prop + N' = replace( replace(''' + @del_prop_val + ''',''%FACTS%'', isnull(mods.dst_val,''0'') ), ''%TIME%'', convert(nvarchar,getdate()) )
		from ' + @trg_tbl + ' mbr inner join ' + @mods_tbl + ' mods
		on mbr.id = mods.id
		and mods.mod_type = ''DEL''
		and mods.dim_nm = ''' + @dim_nm + ''''

		print @sql
		exec (@sql)		
end

/*

select * from autometa_mods_5 where mod_type = 'del'
select * from mbrCustomer where not_in_source is not null

insert autometa_mods_18 (mod_type, dim_nm, id)
select 'DEL', 'Customer', 'VVP07_CP90'

insert autometa_mods_18 (mod_type, dim_nm, id)
select 'DEL', 'Customer', 'C170_100399'

select * from autometa_mods_5

select * from mbrCustomer where calc = 'n'

select count(*) from tblFactSales 

insert autometa_mods_5 (mod_type, dim_nm, id)
select 'DEL', 'Customer', 'C101_101'
*/
/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_ProcessMods]    Script Date: 08/13/2008 01:17:52 ******/
SET ANSI_NULLS ON
