﻿CREATE PROCEDURE [dbo].[Load_BPC_DW_DATA]
AS
-- Procedure to load bpc_dw_file1 data to bpc_dw_data
-- Created by Terry Deck
-- Jun 10, 2009
-- Modified by Terry Deck
-- Apr 13, 2010 
-- changed tables for new naming convention we used in BPC 7.5
BEGIN
DECLARE @v_primary_agent_chf_district VARCHAR(300)
DECLARE @v_product VARCHAR(50)
DECLARE @v_effective_date VARCHAR(10)
DECLARE @v_region VARCHAR(300)
DECLARE @v_f_pol_type_new_ytd_cnt NUMERIC(15)
DECLARE @v_f_pol_type_inforce_cur_cnt NUMERIC(15)
DECLARE @v_f_claim_coverage_cnt NUMERIC(15)
DECLARE @v_f_new_prem_ytd_amt NUMERIC(15)
DECLARE @v_f_new_prem_adj_ytd_amt NUMERIC(15)

DECLARE rec_csr CURSOR 
FOR SELECT LTRIM(primary_agent_chf_district), ltrim(product_type), effective_date,
             lTRIM (region), pol_type_new_ytd_cnt, pol_type_inf_cur_cnt,
             claim_cov_cnt, new_prem_ytd_amt, new_prem_adj_ytd_amt
FROM dbo.c_tblBPC_DW_FILE1

OPEN rec_csr

FETCH NEXT FROM rec_csr
INTO @v_primary_agent_chf_district, @v_product, @v_effective_date,
     @v_region, @v_f_pol_type_new_ytd_cnt, @v_f_pol_type_inforce_cur_cnt,
     @v_f_claim_coverage_cnt, @v_f_new_prem_ytd_amt,
     @v_f_new_prem_adj_ytd_amt

WHILE @@FETCH_STATUS = 0
BEGIN

      IF @v_effective_date < '2006-01-01'
         BEGIN
  	        INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
            VALUES (@v_primary_agent_chf_district, @v_product,'F Pol Type New Ytd Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_pol_type_new_ytd_cnt)

            INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
            VALUES (@v_primary_agent_chf_district, @v_product,'F Pol Type Inforce Current Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_pol_type_inforce_cur_cnt)

            INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
            VALUES (@v_primary_agent_chf_district, @v_product,'F Claim on Coverage Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_claim_coverage_cnt)

            INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
            VALUES (@v_primary_agent_chf_district, @v_product,'F New Premium Ytd Amt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_new_prem_ytd_amt)
                     
            INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
            VALUES (@v_primary_agent_chf_district, @v_product,'F New Premium Adjust Ytd Amt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_new_prem_adj_ytd_amt)
         END    
      ELSE IF @v_effective_date > '2006-01-01'
         BEGIN
            IF @v_product <> 'Regular Auto'
               BEGIN
                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F Pol Type New Ytd Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_pol_type_new_ytd_cnt)

                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F Pol Type Inforce Current Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_pol_type_inforce_cur_cnt)

                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F Claim on Coverage Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_claim_coverage_cnt)

                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F New Premium Ytd Amt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_new_prem_ytd_amt)
                        
                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F New Premium Adjust Ytd Amt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4),@v_f_new_prem_adj_ytd_amt)
               END
            ELSE
               BEGIN
                  INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
                  VALUES (@v_primary_agent_chf_district, @v_product,'F Claim on Coverage Cnt', substring(@v_effective_date,6,2)+substring(@v_effective_date,1,4), @v_f_claim_coverage_cnt)
               END
         END   

FETCH NEXT FROM rec_csr
INTO @v_primary_agent_chf_district, @v_product, @v_effective_date,
     @v_region, @v_f_pol_type_new_ytd_cnt, @v_f_pol_type_inforce_cur_cnt,
     @v_f_claim_coverage_cnt, @v_f_new_prem_ytd_amt,
     @v_f_new_prem_adj_ytd_amt
         
END
close rec_csr
deallocate rec_csr
END