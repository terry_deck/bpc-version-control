﻿







------------------------------CLIC BUDGET DOWNLOAD------------------------------------------------------
---THIS PROCEDURE IS USED TO FEED BPC THE CLIC ALLOCATIONS FROM ORACLE.
---1) CALLS THE ORACLE GL SOURCE TABLES DIRECTLY IN ORACLE USING OPENQUERY
--- CHANGE LOG
--- 11/05/2012 T Deck
--- Procedure created
---------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_CLIC_BUDGET]
AS
INSERT INTO dbo.c_tblBPCOracleBudget (ledger_id,last_update_date,code_combination_id,
begin_balance_dr,begin_balance_cr,period_net_dr,period_net_cr,period_name,period_num,
period_year,currency_code,actual_flag,company,cost_centre,account,interco,
province,program,product,lob,par,project,treaty,reinsurer,loss_year,account_type,budget_version_id)
SELECT *
FROM openquery(PROD_LINK,'   select a.ledger_id,a.last_update_date,a.code_combination_id,a.begin_balance_dr, 
a.begin_balance_cr,a.period_net_dr,a.period_net_cr,a.period_name,a.period_num, 
a.period_year,a.currency_code,a.actual_flag,b.segment1,b.segment2,b.segment3, 
b.segment4,b.segment5,b.segment6,b.segment7,b.segment8,b.segment9,b.segment10, 
b.segment11,b.segment12,b.segment13,b.account_type,a.budget_version_id 
from GL.GL_BALANCES a , GL.GL_CODE_COMBINATIONS b
where a.period_year = 2015 
and a.currency_code = ''CAD''
and a.actual_flag =''B''
and b.segment1 = ''060''
and b.segment3 in (''601800'',''601810'',''608800'',''608810'',''609810'',''609800'',''609820'',
''617800'',''617810'',''606800'',''606810'',''606820'',''606830'',''606840'',''608832'',''609842'',
''609832'',''617832'',''606860'',''606862'',''606864'',''606866'')
and (a.begin_balance_cr <> 0 or a.begin_balance_dr <> 0 or a.period_net_cr <> 0 
or a.period_net_dr <> 0)
and a.budget_version_id in (11086)
and A.CODE_COMBINATION_ID = b.code_combination_id
and b.segment1 <> ''T'' and b.segment2 <> ''T'' and b.segment3 <> ''T'' 
and b.segment4 <> ''T'' and b.segment5 <> ''T'' and b.segment6 <> ''T'' 
and b.segment7 <> ''T'' and b.segment8 <> ''T'' and b.segment9 <> ''T'' 
and b.segment10<> ''T'' and b.segment11 <> ''T'' and b.segment12 <> ''T'' 
and b.segment13 <> ''T'' 
order by a.ledger_id, a.code_combination_id desc')		
--11086 is CGC 2015 BUDGET  10086 is CGC 2014 budget 8086 is CGC 2013 Budget 
--6086 is cgc 2012 budget  5088 is cgc 2011 conv  5089 is cgl 2011 conv




