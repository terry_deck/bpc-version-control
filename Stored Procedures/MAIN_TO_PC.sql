﻿

-- Added selection for Company and code to process Coseco (045_COM)
-- differently than CGIC (021_COM) for Coseco PremClaim project
-- T Deck January 2015


CREATE procedure [dbo].[MAIN_TO_PC] (@time_id nvarchar(8), @Comp nvarchar(7))
 
as	
DECLARE @timeid nvarchar(8)
DECLARE @cmp nvarchar(7)
SET @timeid = @time_id
SET @cmp = @Comp

IF @cmp = '045_COM'
BEGIN
-- This select summarizes all the data from all reinsurer segments to one with 
-- no reinsurer
   Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, 
   PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, 
   SUM(SIGNEDDATA) SIGNEDDATA
   FROM
   (
-- This select summarizes all the data from the 3 fact tables into one record 
-- per unique intersection
      Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, 
      PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, 
      SUM(SIGNEDDATA) SIGNEDDATA
      FROM
   (
-- This select gets all data records for the required time from all 3 fact tables
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, 
         PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, 
         REINSURER, SIGNEDDATA
         FROM TBLFACTMAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '045_COM'
         and ACCOUNT in ('113000_acc','114300_acc','115300_acc','115330_acc',
            '115335_acc','115340_acc','117000_acc','117010_acc','117102_acc',
            '204100_acc','204107_acc','205100_acc','205101_acc','211000_acc',
            '401000_acc','401001_acc','401046_acc','401200_acc','401300_acc',
            '401305_acc','402000_acc','402110_acc','402200_acc','402300_acc',
            '501115_acc','501200_acc','501300_acc','502000_acc','502060_acc',
			'502064_acc','502066_acc','502068_acc','502070_acc','502072_acc',
			'502074_acc','502079_acc','502100_acc','502109_acc','502118_acc',
			'502119_acc','502129_acc','502200_acc','502300_acc','502305_acc',
			'502309_acc','502310_acc','502315_acc','502319_acc','502320_acc',
			'502325_acc','502329_acc','503000_acc','503010_acc','503012_acc',
			'503016_acc','503018_acc','503100_acc','503102_acc','503103_acc',
			'503109_acc','503112_acc','503120_acc','503200_acc','503210_acc',
			'503300_acc','503302_acc','503303_acc','503309_acc','503310_acc',
			'503320_acc','503340_acc','503350_acc','504100_acc','504102_acc',
			'504103_acc','504110_acc','504112_acc','504113_acc','504120_acc',
			'504130_acc','504300_acc','504302_acc','504303_acc','504310_acc',
			'504312_acc','504500_acc','504800_acc','601100_acc','601110_acc',
			'601200_acc','601310_acc','601400_acc','601410_acc','601415_acc',
			'601420_acc','601440_acc','601460_acc','602000_acc','602010_acc',
			'602020_acc','602102_acc','602103_acc','602104_acc','602200_acc',
			'603000_acc','603030_acc','603040_acc','603050_acc','604000_acc',
			'604010_acc','604020_acc','606000_acc','606010_acc','606040_acc',
			'606050_acc','606100_acc','606150_acc','606151_acc','606160_acc',
			'606170_acc','606200_acc','606210_acc','606220_acc','606250_acc',
			'606260_acc','606290_acc','606300_acc','606310_acc','606320_acc',
			'606330_acc','606340_acc','606350_acc','606360_acc','606370_acc',
			'606385_acc','606390_acc','606503_acc',
			--- added the following accounts per Luda 26-05-2015 ---
			'113010_acc','115309_acc','117103_acc','204102_acc','204103_acc',
			'204109_acc','211200_acc',
			--- added the following accounts per Luda 02-06-2015
			'204119_acc','227001_acc')
         Union all
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, SIGNEDDATA
         FROM TBLFAC2MAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '045_COM'
         and ACCOUNT in ('113000_acc','114300_acc','115300_acc','115330_acc',
            '115335_acc','115340_acc','117000_acc','117010_acc','117102_acc',
            '204100_acc','204107_acc','205100_acc','205101_acc','211000_acc',
            '401000_acc','401001_acc','401046_acc','401200_acc','401300_acc',
            '401305_acc','402000_acc','402110_acc','402200_acc','402300_acc',
            '501115_acc','501200_acc','501300_acc','502000_acc','502060_acc',
			'502064_acc','502066_acc','502068_acc','502070_acc','502072_acc',
			'502074_acc','502079_acc','502100_acc','502109_acc','502118_acc',
			'502119_acc','502129_acc','502200_acc','502300_acc','502305_acc',
			'502309_acc','502310_acc','502315_acc','502319_acc','502320_acc',
			'502325_acc','502329_acc','503000_acc','503010_acc','503012_acc',
			'503016_acc','503018_acc','503100_acc','503102_acc','503103_acc',
			'503109_acc','503112_acc','503120_acc','503200_acc','503210_acc',
			'503300_acc','503302_acc','503303_acc','503309_acc','503310_acc',
			'503320_acc','503340_acc','503350_acc','504100_acc','504102_acc',
			'504103_acc','504110_acc','504112_acc','504113_acc','504120_acc',
			'504130_acc','504300_acc','504302_acc','504303_acc','504310_acc',
			'504312_acc','504500_acc','504800_acc','601100_acc','601110_acc',
			'601200_acc','601310_acc','601400_acc','601410_acc','601415_acc',
			'601420_acc','601440_acc','601460_acc','602000_acc','602010_acc',
			'602020_acc','602102_acc','602103_acc','602104_acc','602200_acc',
			'603000_acc','603030_acc','603040_acc','603050_acc','604000_acc',
			'604010_acc','604020_acc','606000_acc','606010_acc','606040_acc',
			'606050_acc','606100_acc','606150_acc','606151_acc','606160_acc',
			'606170_acc','606200_acc','606210_acc','606220_acc','606250_acc',
			'606260_acc','606290_acc','606300_acc','606310_acc','606320_acc',
			'606330_acc','606340_acc','606350_acc','606360_acc','606370_acc',
			'606385_acc','606390_acc','606503_acc',
			--- added the following accounts per Luda 26-05-2015 ---
			'113010_acc','115309_acc','117103_acc','204102_acc','204103_acc',
			'204109_acc','211200_acc',
			--- added the following accounts per Luda 02-06-2015
			'204119_acc','227001_acc')
         Union all
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, SIGNEDDATA
         FROM TBLFACTWBMAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '045_COM'
         and ACCOUNT in ('113000_acc','114300_acc','115300_acc','115330_acc',
            '115335_acc','115340_acc','117000_acc','117010_acc','117102_acc',
            '204100_acc','204107_acc','205100_acc','205101_acc','211000_acc',
            '401000_acc','401001_acc','401046_acc','401200_acc','401300_acc',
            '401305_acc','402000_acc','402110_acc','402200_acc','402300_acc',
            '501115_acc','501200_acc','501300_acc','502000_acc','502060_acc',
			'502064_acc','502066_acc','502068_acc','502070_acc','502072_acc',
			'502074_acc','502079_acc','502100_acc','502109_acc','502118_acc',
			'502119_acc','502129_acc','502200_acc','502300_acc','502305_acc',
			'502309_acc','502310_acc','502315_acc','502319_acc','502320_acc',
			'502325_acc','502329_acc','503000_acc','503010_acc','503012_acc',
			'503016_acc','503018_acc','503100_acc','503102_acc','503103_acc',
			'503109_acc','503112_acc','503120_acc','503200_acc','503210_acc',
			'503300_acc','503302_acc','503303_acc','503309_acc','503310_acc',
			'503320_acc','503340_acc','503350_acc','504100_acc','504102_acc',
			'504103_acc','504110_acc','504112_acc','504113_acc','504120_acc',
			'504130_acc','504300_acc','504302_acc','504303_acc','504310_acc',
			'504312_acc','504500_acc','504800_acc','601100_acc','601110_acc',
			'601200_acc','601310_acc','601400_acc','601410_acc','601415_acc',
			'601420_acc','601440_acc','601460_acc','602000_acc','602010_acc',
			'602020_acc','602102_acc','602103_acc','602104_acc','602200_acc',
			'603000_acc','603030_acc','603040_acc','603050_acc','604000_acc',
			'604010_acc','604020_acc','606000_acc','606010_acc','606040_acc',
			'606050_acc','606100_acc','606150_acc','606151_acc','606160_acc',
			'606170_acc','606200_acc','606210_acc','606220_acc','606250_acc',
			'606260_acc','606290_acc','606300_acc','606310_acc','606320_acc',
			'606330_acc','606340_acc','606350_acc','606360_acc','606370_acc',
			'606385_acc','606390_acc','606503_acc',
			--- added the following accounts per Luda 26-05-2015 ---
			'113010_acc','115309_acc','117103_acc','204102_acc','204103_acc',
			'204109_acc','211200_acc',
			--- added the following accounts per Luda 02-06-2015
			'204119_acc','227001_acc')
      ) tmp2
      group by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER
   ) tmp
   group by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT
   order by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT
END
ELSE
BEGIN
-- This select summarizes all the data from all reinsurer segments to one with 
-- no reinsurer
   Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, 
   PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, 
   SUM(SIGNEDDATA) SIGNEDDATA
   FROM
   (
-- This select summarizes all the data from the 3 fact tables into one record 
-- per unique intersection
      Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, 
      PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, 
      SUM(SIGNEDDATA) SIGNEDDATA
      FROM
   (
-- This select gets all data records for the required time from all 3 fact tables
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, 
         PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, 
         REINSURER, SIGNEDDATA
         FROM TBLFACTMAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '021_COM'
         and ACCOUNT in ('117000_acc','117010_acc','211000_acc','401000_acc',
            '401001_acc','401046_acc','401200_acc','401300_acc','401305_acc',
			'402000_acc','402110_acc','402200_acc','402300_acc','503000_acc',
			'501115_acc','502000_acc','502060_acc','502064_acc','502066_acc',
			'502100_acc','502109_acc','502118_acc','502119_acc','502070_acc',
			'502072_acc','502074_acc','502079_acc','504103_acc','504113_acc',
			'504100_acc','504102_acc','504110_acc','504112_acc','504120_acc',
			'504130_acc','502068_acc','502129_acc','503010_acc','503012_acc',
			'503109_acc','503016_acc','503018_acc','503103_acc','503100_acc',
			'503102_acc','503112_acc','503120_acc','504500_acc','504800_acc',
			'501200_acc','503200_acc','502200_acc','503210_acc','501300_acc',
			'503300_acc','502305_acc','502319_acc','502329_acc','502300_acc',
			'502309_acc','502310_acc','502315_acc','502320_acc','504300_acc',
			'504302_acc','504310_acc','503302_acc','503350_acc','503340_acc',
			'503309_acc','503310_acc','503320_acc','S50011_acc','601100_acc',
			'601110_acc','602000_acc','602103_acc','601200_acc','601310_acc',
			'601400_acc','601410_acc','601415_acc','601420_acc','601460_acc',
			'601440_acc','603000_acc','603030_acc','603040_acc','603050_acc',
			'604000_acc','604010_acc','604020_acc','602010_acc','602020_acc',
			'602104_acc','602200_acc','606000_acc','606010_acc','606040_acc',
			'606050_acc','606503_acc','606071_acc','606260_acc','606290_acc',
			'606300_acc','606310_acc','606320_acc','606330_acc','606340_acc',
			'606360_acc','606370_acc','606100_acc','606200_acc','606210_acc',
			'606220_acc','606250_acc','606350_acc','606385_acc','606390_acc',
			'606395_acc','606160_acc','606150_acc','606151_acc','606170_acc',
			'602102_acc','117102_acc')
         Union all
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, SIGNEDDATA
         FROM TBLFAC2MAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '021_COM'
         and ACCOUNT in ('117000_acc','117010_acc','211000_acc','401000_acc',
            '401001_acc','401046_acc','401200_acc','401300_acc','401305_acc',
            '402000_acc','402110_acc','402200_acc','402300_acc','503000_acc',
            '501115_acc','502000_acc','502060_acc','502064_acc','502066_acc',
            '502100_acc','502109_acc','502118_acc','502119_acc','502070_acc',
            '502072_acc','502074_acc','502079_acc','504103_acc','504113_acc',
            '504100_acc','504102_acc','504110_acc','504112_acc','504120_acc',
            '504130_acc','502068_acc','502129_acc','503010_acc','503012_acc',
            '503109_acc','503016_acc','503018_acc','503103_acc','503100_acc',
            '503102_acc','503112_acc','503120_acc','504500_acc','504800_acc',
            '501200_acc','503200_acc','502200_acc','503210_acc','501300_acc',
            '503300_acc','502305_acc','502319_acc','502329_acc','502300_acc',
            '502309_acc','502310_acc','502315_acc','502320_acc','504300_acc',
            '504302_acc','504310_acc','503302_acc','503350_acc','503340_acc',
            '503309_acc','503310_acc','503320_acc','S50011_acc','601100_acc',
            '601110_acc','602000_acc','602103_acc','601200_acc','601310_acc',
            '601400_acc','601410_acc','601415_acc','601420_acc','601460_acc',
            '601440_acc','603000_acc','603030_acc','603040_acc','603050_acc',
            '604000_acc','604010_acc','604020_acc','602010_acc','602020_acc',
            '602104_acc','602200_acc','606000_acc','606010_acc','606040_acc',
            '606050_acc','606503_acc','606071_acc','606260_acc','606290_acc',
            '606300_acc','606310_acc','606320_acc','606330_acc','606340_acc',
            '606360_acc','606370_acc','606100_acc','606200_acc','606210_acc',
            '606220_acc','606250_acc','606350_acc','606385_acc','606390_acc',
            '606395_acc','606160_acc','606150_acc','606151_acc','606170_acc',
            '602102_acc','117102_acc')
         Union all
         Select CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER, SIGNEDDATA
         FROM TBLFACTWBMAIN
         where CATEGORY = 'ACTUAL'
         and TIMEID = @timeid
         and COMPANY = '021_COM'
         and ACCOUNT in ('117000_acc','117010_acc','211000_acc','401000_acc',
            '401001_acc','401046_acc','401200_acc','401300_acc','401305_acc',
            '402000_acc','402110_acc','402200_acc','402300_acc','503000_acc',
            '501115_acc','502000_acc','502060_acc','502064_acc','502066_acc',
            '502100_acc','502109_acc','502118_acc','502119_acc','502070_acc',
            '502072_acc','502074_acc','502079_acc','504103_acc','504113_acc',
            '504100_acc','504102_acc','504110_acc','504112_acc','504120_acc',
            '504130_acc','502068_acc','502129_acc','503010_acc','503012_acc',
            '503109_acc','503016_acc','503018_acc','503103_acc','503100_acc',
            '503102_acc','503112_acc','503120_acc','504500_acc','504800_acc',
            '501200_acc','503200_acc','502200_acc','503210_acc','501300_acc',
            '503300_acc','502305_acc','502319_acc','502329_acc','502300_acc',
            '502309_acc','502310_acc','502315_acc','502320_acc','504300_acc',
            '504302_acc','504310_acc','503302_acc','503350_acc','503340_acc',
            '503309_acc','503310_acc','503320_acc','S50011_acc','601100_acc',
            '601110_acc','602000_acc','602103_acc','601200_acc','601310_acc',
            '601400_acc','601410_acc','601415_acc','601420_acc','601460_acc',
            '601440_acc','603000_acc','603030_acc','603040_acc','603050_acc',
            '604000_acc','604010_acc','604020_acc','602010_acc','602020_acc',
            '602104_acc','602200_acc','606000_acc','606010_acc','606040_acc',
            '606050_acc','606503_acc','606071_acc','606260_acc','606290_acc',
            '606300_acc','606310_acc','606320_acc','606330_acc','606340_acc',
            '606360_acc','606370_acc','606100_acc','606200_acc','606210_acc',
            '606220_acc','606250_acc','606350_acc','606385_acc','606390_acc',
            '606395_acc','606160_acc','606150_acc','606151_acc','606170_acc',
            '602102_acc','117102_acc')
      ) tmp2
      group by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT, REINSURER
   ) tmp
   group by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT
   order by CATEGORY, DATASRC, COMPANY, COSTCENTRE, INTCO, LOB, LOSSYEAR, PAR, PRODUCT, PROGRAM, PROJECT, PROVINCE, TREATY, TIMEID, ACCOUNT
END






