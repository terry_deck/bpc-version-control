﻿


------------------------------asp_BPC_DATE_SELECTION------------------------------------------------------
---THIS PROCEDURE IS USED in the batch version of Push to Consol.
---It creates a varchar string containing the month/year to clear and push to the 
---Consol app based on the current month.  This value is returned to the Variable myselection
---in the push to consol package.  This value is used in the selection
---for the clear and push steps. 
---
--- 29/10/2012 T Deck
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_DATE_SELECTION] 
AS
SET NOCOUNT ON;

BEGIN
Declare @curmth int
Declare @curyr int
Declare @lastyr int
Declare @selstring nvarchar(225)
set @curmth = (select month(getdate()))
set @curyr =  (select year(getdate()))
set @lastyr = (select year(getdate())-1)

set @selstring =
  (case
	when @curmth = 1
		then cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN'
	when @curmth = 2
		then cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB'
	when @curmth = 3
		then cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR'
	when @curmth = 4
		then cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR'
	when @curmth = 5
		then cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY'
	when @curmth = 6
		then 
			 cast(@lastyr as char(4))+'.JAN,'+cast(@lastyr as char(4))+'.FEB,'+
			 cast(@lastyr as char(4))+'.MAR,'+cast(@lastyr as char(4))+'.APR,'+
		     cast(@lastyr as char(4))+'.MAY,'+cast(@lastyr as char(4))+'.JUN,'+
		     cast(@lastyr as char(4))+'.JUL,'+cast(@lastyr as char(4))+'.AUG,'+
		     cast(@lastyr as char(4))+'.SEP,'+cast(@lastyr as char(4))+'.OCT,'+		   
		     cast(@lastyr as char(4))+'.NOV,'+cast(@lastyr as char(4))+'.DEC,'+
		     cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN'
	when @curmth = 7
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL'
	when @curmth = 8
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL,'+cast(@curyr as char(4))+'.AUG'
	when @curmth = 9
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL,'+cast(@curyr as char(4))+'.AUG,'+
		     cast(@curyr as char(4))+'.SEP'
	when @curmth = 10
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL,'+cast(@curyr as char(4))+'.AUG,'+
		     cast(@curyr as char(4))+'.SEP,'+cast(@curyr as char(4))+'.OCT'
	when @curmth = 11
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL,'+cast(@curyr as char(4))+'.AUG,'+
		     cast(@curyr as char(4))+'.SEP,'+cast(@curyr as char(4))+'.OCT,'+
		     cast(@curyr as char(4))+'.NOV'
	when @curmth = 12
		then cast(@curyr as char(4))+'.JAN,'+cast(@curyr as char(4))+'.FEB,'+
		     cast(@curyr as char(4))+'.MAR,'+cast(@curyr as char(4))+'.APR,'+ 		     
		     cast(@curyr as char(4))+'.MAY,'+cast(@curyr as char(4))+'.JUN,'+
		     cast(@curyr as char(4))+'.JUL,'+cast(@curyr as char(4))+'.AUG,'+
		     cast(@curyr as char(4))+'.SEP,'+cast(@curyr as char(4))+'.OCT,'+
		     cast(@curyr as char(4))+'.NOV,'+cast(@curyr as char(4))+'.DEC'
	END  
	)
	
	Select @selstring as selstring

END

