﻿CREATE proc [dbo].[csp_Aud_ProcLogUpd] (
	  @aud_proc_log_id int
	, @rec_count bigint = 0
	, @sql_err_num int = 0
	, @sql_err_text nvarchar(500) = NULL
	, @status char(1) = 'F'
	, @comments text = NULL
) AS

set nocount on

update aud_proc_log
	set 
	  rec_count = @rec_count
	, sql_err_num = @sql_err_num
	, sql_err_text = @sql_err_text
	, status = @status
	, comments = @comments
	, updt_dtm = getdate()
	where aud_proc_log_id = @aud_proc_log_id

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_Aud_ProcLogUpd]
		@aud_proc_log_id = 1,
		@rec_count = 0,
		@sql_err_num = NULL,
		@sql_err_text = NULL,
		@status = N'C',
		@comments = N'worked good'

SELECT	'Return Value' = @return_value

*/

/*************************************** Create Stored Procedures **********************************************/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetControlInfo]    Script Date: 08/13/2008 01:15:30 ******/
SET ANSI_NULLS ON
