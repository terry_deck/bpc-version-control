﻿CREATE proc [dbo].[csp_Aud_MainLogUpd] (
	  @aud_main_log_id int
	, @rec_count bigint = 0
	, @status char(1) = 'F'
	, @comments text = NULL
) AS

set nocount on

update aud_main_log
	set 
	  rec_count = @rec_count
	, status = @status
	, comments = @comments
	, updt_dtm = getdate()
	where aud_main_log_id = @aud_main_log_id

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_Aud_MainLogUpd]
		@aud_main_log_id = 1,
		@rec_count = 50,
		@status = N'C',
		@comments = N'I''m done.'

SELECT	'Return Value' = @return_value

*/

