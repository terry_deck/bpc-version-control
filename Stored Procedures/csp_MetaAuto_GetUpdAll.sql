﻿CREATE PROC [dbo].[csp_MetaAuto_GetUpdAll] (
	  @src_tbl sysname = 'metaTmpCustomer'
	, @trg_tbl sysname = 'mbrCustomer'
	, @dim_nm nvarchar(50) = 'Customer'
	, @mods_tbl sysname OUTPUT
) as

/******
 Notes:

	- Generates a table called autometa_chg_fld
	- Contains ALL differences between metaTmp{DIM} and mbr{DIM}
	  provided that the field name exists at least one time in mbrzzzMetaMapping
	- hier_lvl will always be NULL
	- Uniqueness is defined by: dim_nm, fld_nm, id (there could
	  be multiple records for the same ID because two properties
	  may have changed for a dimension member
*/

set nocount on

-- delcare local vars
declare 
	  @cur_dim nvarchar(50)
	, @trg_fld sysname
	, @upd_where nvarchar(1000)
	, @tmp_tbl sysname
	, @sql nvarchar(4000)
	, @max_len nvarchar(5)

-- declare/open cursor for the fields
declare upd_flds cursor fast_forward for
	select dim_nm, trg_fld 
	from mbrzzzMetaMapping 
	where updatable = 1
	  and dim_nm = @dim_nm
	group by dim_nm, trg_fld
	order by dim_nm;
open upd_flds;

-- get the first row
fetch next from upd_flds
	into @cur_dim, @trg_fld;

-- compare field for the certain level and write the chg record if it differs
while @@fetch_status = 0 begin
	
	set @sql = N'-- ' + @trg_fld + N' test
	insert ' + @mods_tbl + N' (mod_type, dim_nm, fld_nm, id, src_val, dst_val)	
		select N''UPD'', N''' + @cur_dim + N''', N''' + @trg_fld + N''', target.ID, source.' + @trg_fld + N', target.' + @trg_fld + N'
		from ' + @trg_tbl + N' target inner join ' + @src_tbl + N' source
		  on target.ID = source.ID
		where source.' + @trg_fld + N'<>target.' + @trg_fld

--	print @sql
	exec( @sql )

	
	fetch next from upd_flds
		into @cur_dim, @trg_fld;

end

close upd_flds
deallocate upd_flds

/* Sample Call:

DECLARE	@return_value int,
		@mods_tbl sysname

delete from autometa_mods_5

EXEC	@return_value = [dbo].[csp_MetaAuto_GetChgAll]
		@src_tbl = metaTmpCustomer,
		@trg_tbl = mbrCustomer,
		@dim_nm = N'Customer',
		@mods_tbl = 'autometa_mods_5'

SELECT	'Return Value' = @return_value
exec ('select * from autometa_mods_5')

*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetUpd]    Script Date: 08/13/2008 01:17:37 ******/
SET ANSI_NULLS ON
