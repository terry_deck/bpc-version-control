﻿
------------------------------asp_DATA_CHECK_INC------------------------------------------------------
---THIS procedure is used in the incremental actuals load process.
---It tests the data which has been downloaded from Oracle on that particular day
---and compares it to the data in BPC. If there are differences they are written
---to DBO.TMPBPC_DATACHECK_INC which is then used to create the data check
---report in reporting services
--- 29/10/2012 T Deck
---
--- 15/04/2014 T Deck
--- Removed reference to consol ledger for new consol implementation 2013-0054
--------------------------------------------------------------------------------------------------




CREATE procedure [dbo].[asp_DATA_CHECK_INC]

AS

--------------------------------BEGIN DATA CHECK-----------------------------------------------------

IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES	 WHERE TABLE_NAME = 'TMPBPC_DATACHECK_INC')

BEGIN
CREATE TABLE dbo.TMPBPC_DATACHECK_INC 

--CREATE TEMPORARY TABLE FOR THE REPORT DATA
(
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[TIMEID]      NVARCHAR(20),
[CATEGORY]    NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,10),
[STATUS]      NVARCHAR(21))

END
ELSE
BEGIN
TRUNCATE TABLE dbo.TMPBPC_DATACHECK_INC
END
 
BEGIN
		
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------BEGIN INTEGRITY CHECK-----------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
INSERT INTO DBO.TMPBPC_DATACHECK_INC ([COMPANY],[ACCOUNT],[COSTCENTRE],[INTCO],[LOB],[LOSSYEAR],[PAR],[PRODUCT],[PROGRAM],[PROJECT],[PROVINCE],[REINSURER],[TREATY],[TIMEID],[CATEGORY],[SIGNEDDATA],[STATUS])
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID, Category,
		SIGNEDDATA,'in Oracle but not BPC' AS STATUS		
FROM(
		SELECT a.Company,a.Account,a.CostCentre,a.Intco,a.Lob,a.Lossyear,a.Par,a.Product,a.Program,a.Project,a.Province,
		a.Reinsurer,a.Treaty,a.TIMEID,a.CATEGORY,a.SIGNEDDATA
		FROM dbo.aBPC_STAGE a
		WHERE a.SIGNEDDATA <> 0
		and CATEGORY in ('ACTUAL')

		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,sum(SIGNEDDATA)
from
(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				and substring(TIMEID,1,4)>= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)
union all
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFAC2MAIN
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				and substring(TIMEID,1,4)>= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)
union all
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM tblFACTWBMain
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				and substring(TIMEID,1,4)>= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)) A
GROUP by Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY)

	)W
UNION
----------------------------Tests for records in BPC that are not in Oracle-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,
		SIGNEDDATA,'in BPC but not Oracle' AS STATUS
FROM(
		SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,sum(SIGNEDDATA) as signeddata
from
(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				AND LEFT(TIMEID,4) >= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)
				
union all
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFAC2MAIN
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				AND LEFT(TIMEID,4) >= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)
				
union all
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM tblFACTWBMain
				WHERE CATEGORY in ('ACTUAL')
				and DATASRC = 'ORACLE'
				AND LEFT(TIMEID,4) >= (SELECT MIN(PERIOD_YEAR) from aBPC_STAGE)) A
GROUP by Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM dbo.aBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL
				)
		)x
--order by STATUS,DIMENSION,MEMBER
END




