﻿CREATE proc [dbo].[csp_MetaAuto_GetDel] (
	  @tmp_tbl sysname = 'metaTmpCustomer'
	, @trg_tbl sysname = 'mbrCustomer'
	, @dim_nm nvarchar(50) = 'Customer'
	, @mods_tbl sysname = 'autometa_mods_1'
) as

declare @sql nvarchar(500)

set @sql = N'insert ' + @mods_tbl + N'(mod_type, dim_nm, ID)
	select ''DEL'', N''' + @dim_nm + N''', m.ID
	from ' + @trg_tbl + N' m
	left outer join ' + @tmp_tbl + N' t
	  on m.ID = t.ID
	where t.ID is null'

--print @sql
exec (@sql)

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_MetaAuto_GetDel]
		@tmp_tbl = metaTmpCustomer,
		@trg_tbl = mbrCustomer,
		@dim_nm = N'Customer',
		@mods_tbl = autometa_mods_5

SELECT	'Return Value' = @return_value

*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_Build_Parent_Child]    Script Date: 08/13/2008 01:13:03 ******/
SET ANSI_NULLS ON
