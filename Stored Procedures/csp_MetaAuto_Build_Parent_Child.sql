﻿CREATE proc [dbo].[csp_MetaAuto_Build_Parent_Child] (
	  @MetaControlID nvarchar(20)
	, @main_log_id int  = 1 -- the main audit log id
) as

--Validate Data
--Email results

--DECLARE VARIABLES & CONSTANTS
DECLARE 
	  @dim_nm sysname 
	, @src_tbl sysname
	, @tmp_tbl sysname
	, @trg_tbl sysname
	, @control_tbl sysname
	, @SQL nvarchar(max)
	, @hier_lvl int
	, @src_fld nvarchar(max)
	, @trg_fld sysname
	, @src_flds nvarchar(max)
	, @grp_flds nvarchar(max)
	, @trg_flds nvarchar(max)
	, @ins_where_fld nvarchar(max)
	, @ins_where nvarchar(max)
	, @src_where_clause nvarchar(max)
	, @grp_clause nvarchar(max)
	, @counter int
	, @comma nvarchar(1)
	, @fld_len int
	, @proc_log_id int
	, @lvl_log_text nvarchar(500)
	, @dummy as int


EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_Build_Parent_Child
	, @action_item = N'validate parameters'

--ASSIGNMENTS
	SELECT @dim_nm = dim_nm from mbrzzzMetaControl where ID=@MetaControlID;  PRINT 'dim_nm=' + @dim_nm 
	SELECT @src_tbl=src_tbl_nm FROM mbrzzzMetaControl WHERE ID=@MetaControlID; PRINT 'src_tbl=' + @src_tbl
	SELECT @tmp_tbl=tmp_tbl_nm FROM mbrzzzMetaControl WHERE ID=@MetaControlID; PRINT 'tmp_tbl=' + @tmp_tbl
	SELECT @trg_tbl=trg_tbl_nm FROM mbrzzzMetaControl WHERE ID=@MetaControlID; PRINT 'trg_tbl=' + @trg_tbl
	SELECT @src_where_clause=src_where_clause FROM mbrzzzMetaControl WHERE ID=@MetaControlID; PRINT 'src_where_clause=' + @src_where_clause


--VALIDATE VARIABLES
	IF object_id(@src_tbl) IS NULL
		BEGIN
			PRINT 'Source table [' + @src_tbl + '] does not exist';
			RETURN;
		END

	IF object_id(@trg_tbl) IS NULL 
		BEGIN
			PRINT 'Target table [' + @trg_tbl + '] does not exist';
			RETURN;
		END


EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_Build_Parent_Child
	, @action_item = N'validate target fields'

-- VALIDATE target fields
	
	set @sql = 'declare @bad_fld_count int
	select @bad_fld_count = count(*) from mbrzzzMetaMapping where ID=''' + @MetaControlID + ''' and src_fld not in (select column_name from information_schema.columns where table_name = ''' + @src_tbl + ''')
	if @bad_fld_count <> 0 
	begin
		select ID, src_fld, ''bad source field'' reason from mbrzzzMetaMapping where src_fld not in (select column_name from information_schema.columns where table_name = ''' + @src_tbl + ''')
		return
	end 	
	'
	exec(@sql)
	  
EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_Build_Parent_Child
	, @action_item = N'validate source fields'

-- VALIDATE source fields
	set @sql = 'declare @bad_fld_count int
	select @bad_fld_count = count(*) from mbrzzzMetaMapping where control_id like ''' + @MetaControlID + ''' AND trg_fld not like ''%''''%'' and trg_fld not in (select column_name from information_schema.columns where table_name = ''' + @trg_tbl + ''')
	if @bad_fld_count <> 0 
	begin
		select ID, trg_fld, ''bad target field'' reason from mbrzzzMetaMapping where control_id like ''' + @MetaControlID + ''' AND trg_fld not in (select column_name from information_schema.columns where table_name = ''' + @trg_tbl + ''')
		return
	end 	
	'
	exec(@sql)

EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_Build_Parent_Child
	, @action_item = N'rebuild Tmp table'

--REBUILD TEMP TABLE
	IF OBJECT_ID(@tmp_tbl) is not null begin
		PRINT	'DROPPING TEMP TABLE: ' + @tmp_tbl
		SET @SQL = 'DROP TABLE ' + @tmp_tbl;
		EXEC (@SQL)
	END

	PRINT	'REBUILDING TEMP TABLE: ' + @tmp_tbl + ' FROM ' + @trg_tbl + ' (CREATES STRUCTURE BUT NO RECORDS.  SHOULD SAY BELOW "0 row(s) affected")'
	SET @SQL = 'SELECT * INTO ' + @tmp_tbl + ' FROM ' + @trg_tbl + ' WHERE 1=0';
	EXEC (@SQL)

	PRINT	''	
	PRINT	'DROPPING SEQ COLUMN IN TABLE: ' + @tmp_tbl
	SET @SQL = 'ALTER TABLE ' + @tmp_tbl + ' DROP COLUMN SEQ'
	EXEC (@SQL)

	PRINT	''	
	PRINT	'ADDING meta_num_distinct_rec COLUMN IN TABLE: ' + @tmp_tbl
	SET @SQL = 'ALTER TABLE ' + @tmp_tbl + ' ADD META_NUM_DISTINCT_RECS NUMERIC(18,0) NULL '
	EXEC (@SQL)

	PRINT	''	
	PRINT	'ADDING autometa_hier_lvl COLUMN IN TABLE: ' + @tmp_tbl
	SET @SQL = 'ALTER TABLE ' + @tmp_tbl + ' ADD autometa_hier_lvl INT NULL '
	EXEC (@SQL)

EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

	--CREATE & EXECUTE DYNAMIC SQL STATEMENTS (ONE FOR EACH LEVEL OF THE HIERARCHY) TO INSERT RECORDS FROM THE SOURCE TABLE INTO THE TEMP TABLE
	DECLARE LoopThroughLevsInHier CURSOR FAST_FORWARD FOR
		SELECT	 DISTINCT hier_lvl 
		FROM	 mbrzzzMetaMapping
		WHERE	 dim_nm like @dim_nm AND control_id like @MetaControlID
		ORDER BY hier_lvl

	OPEN LoopThroughLevsInHier;
	FETCH NEXT FROM LoopThroughLevsInHier INTO @hier_lvl;

	--LOOP THROUGH EACH HIERARCHY LEVEL
	WHILE @@fetch_status = 0 BEGIN
		
		set @lvl_log_text = 'building level ' + convert(nvarchar, @hier_lvl)

		EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_Build_Parent_Child
			, @action_item = @lvl_log_text


		DECLARE LoopThroughColsInLev CURSOR FAST_FORWARD FOR
			SELECT	 src_fld, trg_fld, ins_where
			FROM	 mbrzzzMetaMapping
			WHERE	 dim_nm=@dim_nm AND hier_lvl=@hier_lvl AND control_id like @MetaControlID 

		OPEN LoopThroughColsInLev
		FETCH NEXT FROM LoopThroughColsInLev INTO @src_fld, @trg_fld, @ins_where_fld
		select @counter=0, @trg_flds='', @src_flds='', @grp_flds='', @ins_where=''

		--LOOP THROUGH EACH FIELD IN THE HIERARCHY
		WHILE @@fetch_status = 0 BEGIN

			select @fld_len = isnull(CHARACTER_MAXIMUM_LENGTH,50) from information_schema.columns where table_name = @trg_tbl and column_name = @trg_fld

			SET @trg_flds = @trg_flds + @trg_fld + ','
			SET @src_flds = @src_flds + 'CONVERT(NVARCHAR(' + convert(varchar,@fld_len) + '),' + @src_fld + '),'

			IF NOT @ins_where_fld = '' SET @ins_where = @ins_where + ' AND ' + @ins_where_fld 
			
			IF NOT ((LEFT(@src_fld,1)='''' OR LEFT(@src_fld,2)='N''') AND RIGHT(@src_fld,1)='''')
				SET @grp_flds = @grp_flds + ', CONVERT(NVARCHAR(' + convert(varchar,@fld_len) + '),' + @src_fld + ')'

			FETCH NEXT FROM LoopThroughColsInLev INTO @src_fld, @trg_fld, @ins_where_fld

		END

		set @src_flds = @src_flds + convert(varchar,@hier_lvl) -- substring(@src_flds,3,len(@src_flds))
		set @grp_flds = substring(@grp_flds,3,len(@grp_flds))
		set @trg_flds = @trg_flds + 'autometa_hier_lvl' -- substring(@trg_flds,3,len(@trg_flds))
		
		if @grp_flds<>'' SET @grp_clause = 'GROUP BY ' + @grp_flds else SET @grp_clause=''

		--PUT TOGETHER DYNAMIC SQL STATEMENT
		SET @SQL='INSERT INTO ' + @tmp_tbl + ' ( ' + @trg_flds + ', META_NUM_DISTINCT_RECS ) 
			SELECT ' + @src_flds + ',COUNT(*) META_NUM_DISTINCT_RECS FROM ' + @src_tbl + ' SOURCE 
			' + @src_where_clause + ' ' + @ins_where + ' 
			' + @grp_clause

		-- select @tmp_tbl tmp_tbl, @trg_flds trg_flds, @src_flds src_flds, @src_tbl src_tbl

		PRINT '' ;PRINT @SQL
		EXEC (@SQL)
		CLOSE LoopThroughColsInLev;	DEALLOCATE LoopThroughColsInLev

		--PRINT 'HIER_LVL=' + convert(nvarchar,@hier_lvl)
		FETCH NEXT FROM LoopThroughLevsInHier INTO @hier_lvl;

		EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

	END

	CLOSE LoopThroughLevsInHier;DEALLOCATE LoopThroughLevsInHier

--VALIDATE NEW DATA

	PRINT 'Removing records with invalid ''ID'' field values (i.e. Null values, Values with special characters, etc.)'
	SET @SQL = 'DELETE FROM ' + @tmp_tbl + '
	WHERE ID IS NULL 
	OR ID=''''
	OR LEN(ID) > 20
	OR CHARINDEX(''&'',ID) <> 0
	OR CHARINDEX(''-'',ID) <> 0
	OR CHARINDEX(''\'',ID) <> 0
	OR CHARINDEX(''/'',ID) <> 0 '

	EXEC (@SQL)
	
--[csp_MetaAuto_Build_Parent_Child] @MetaControlID='Activity'

/*
		INSERT INTO METATMPFUND (ID,EVDESCRIPTION)
		SELECT FUND_ID, CONVERT(NVARCHAR(50),FUND_DESCR) FROM METASRCFUND

SELECT CONVERT(NVARCHAR(50),FUND_DESCR) FROM METASRCFUND WHERE LEN(FUND_DESCR)>50

*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_Validate_Sample]    Script Date: 08/13/2008 01:18:05 ******/
SET ANSI_NULLS ON
