﻿CREATE PROCEDURE [dbo].[Load_BPC_DW_DATA2]
AS
-- Procedure to load bpc_dw_file2 data to bpc_dw_data
-- Created by Terry Deck
-- Jun 10, 2009
-- Modified by Terry Deck
-- Apr 13, 2010 
-- Changed table names for new naming convention we used for BPC 7.5
BEGIN
DECLARE @v_primary_agent_chf_district VARCHAR(300)
DECLARE @v_product VARCHAR(50)
DECLARE @v_year VARCHAR(4)
DECLARE @v_month VARCHAR(2)
DECLARE @v_new_vehicle_count NUMERIC(15)
DECLARE @v_new_policy_count NUMERIC(15)
DECLARE @v_new_premium NUMERIC(15)
DECLARE @v_vif NUMERIC(15)
DECLARE @v_pif NUMERIC(15)

DECLARE rec_csr CURSOR 
FOR SELECT LTRIM(yr), LTRIM(month), LTRIM(primary_agent_chf_district), ltrim(product_type),
             new_vehicle_count, new_policy_count, new_premium, vif, pif
FROM dbo.c_tblBPC_DW_FILE2

OPEN rec_csr

FETCH NEXT FROM rec_csr
INTO @v_year, @v_month, @v_primary_agent_chf_district, @v_product, 
     @v_new_vehicle_count, @v_new_policy_count, @v_new_premium, @v_vif, @v_pif

WHILE @@FETCH_STATUS = 0
BEGIN

   INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
   VALUES (@v_primary_agent_chf_district, @v_product,'New Vehicle Count', @v_month+@v_year, @v_new_vehicle_count)

   INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
   VALUES (@v_primary_agent_chf_district, @v_product,'New Policy Count', @v_month+@v_year, @v_new_policy_count)

   INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
   VALUES (@v_primary_agent_chf_district, @v_product,'New Premium', @v_month+@v_year, @v_new_premium)

   INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
   VALUES (@v_primary_agent_chf_district, @v_product,'VIF', @v_month+@v_year, @v_vif)
                     
   INSERT INTO dbo.c_tblBPC_dw_data (primary_agent_chf_district, prod, acct, effective, cnt)
   VALUES (@v_primary_agent_chf_district, @v_product,'PIF', @v_month+@v_year, @v_pif)
    
      
FETCH NEXT FROM rec_csr
INTO @v_year, @v_month, @v_primary_agent_chf_district, @v_product, 
     @v_new_vehicle_count, @v_new_policy_count, @v_new_premium, @v_vif, @v_pif
         
END
close rec_csr
deallocate rec_csr
END