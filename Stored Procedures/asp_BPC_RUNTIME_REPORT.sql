﻿
------------------------------asp_BPC_RUNTIME_REPORT------------------------------------------------------
---THIS PROCEDURE IS USED in the creation of the Data/Meta-data package run-time report
---It Creates a temp table grouping things by date and package which can't be easily done
---in reporting services at run time
---Creates temp table
---Runs a select on the temp table
---Drops the temp table
--- 31/08/2011 T Deck
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_RUNTIME_REPORT] @ReportMonth int,@ReportMonth2 int, @ReportYear nvarchar(4), @ReportYear2 nvarchar(4)
AS
SET NOCOUNT ON;


BEGIN
IF EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'dbo.temp_runtimes')
  TRUNCATE TABLE dbo.temp_runtimes
ELSE

BEGIN
DECLARE @ddayy int
IF @ReportMonth2 in (4,6,9,11)
   Select @ddayy = 30
ELSE
  BEGIN
      IF @ReportMonth2 != 2 
         BEGIN
            Select @ddayy ='31'
         END
      ELSE
         BEGIN
            IF @ReportYear2 in ('2012','2016','2020','2024','2028','2032','2026','2040')
               BEGIN
                  Select @ddayy = '29'
               END
            ELSE
               BEGIN
                  Select @ddayy ='28'
               END
         END
   END
SELECT 
	 [StartTime]
	,CASE 
	   WHEN (datepart(Hour,starttime)  > 22) 
	   THEN datename(MONTH,dateadd(day,  1, starttime))+ '-'+ datename(DAY,dateadd(day, 1, StartTime))
	   ELSE datename(MONTH,starttime)+ '-'+ datename(DAY,StartTime) 
	   --ELSE datename(MONTH,dateadd(day,  1, starttime))+ '-'+ datename(DAY,dateadd(day, 1, StartTime)) 
	   END AS 'RunDate'
,CASE 
	   WHEN (datepart(Hour,starttime)  > 22) 
	   THEN datename(MONTH,dateadd(day,  1, starttime))
	   ELSE datename(MONTH,starttime) 
	--   ELSE datename(MONTH,dateadd(day,  1, starttime)) 
	   END AS 'RunMonth'
,CASE 
	   WHEN (datepart(Hour,starttime) > 7 and datepart(Hour, starttime) < 22) 
	   THEN 'Day Time' 
	   ELSE 'Night Time'
	   END AS 'RType'
     ,DATEDIFF(SECOND,StartTime,EndTime) AS 'Duration'
     ,CASE 
	   WHEN (datepart(Hour,starttime) >= 6 and datepart(Hour, starttime) <= 8) 
	   THEN PackageID + substring(substring([AnswersToPrompt], (CHARINDEX('ID%',substring(AnswersToPrompt,1,100),1) ), (CHARINDEX('%EMAILTO%',substring(AnswersToPrompt,1,100),1) - (CHARINDEX('ID%',substring(AnswersToPrompt,1,100),1)))),4,15)+' 2nd run'
                   ELSE PackageID + substring(substring([AnswersToPrompt], (CHARINDEX('ID%',substring(AnswersToPrompt,1,100),1) ), (CHARINDEX('%EMAILTO%',substring(AnswersToPrompt,1,100),1) - (CHARINDEX('ID%',substring(AnswersToPrompt,1,100),1)))),4,15) 
                  END as 'Package'
  INTO dbo.temp_runtimes
  FROM dbo.tblDTSLog
  where PackageID in ('Column5 Build BPC Dimension','Download Oracle Shared Hierarchy','BPC_Load_Fact')
  and Appset in ('PROD_COBPC')
  and Status = 'Completed'
  and StartTime>= '2011-06-27 20:00:00.000'
  and (CASE 
	   WHEN (datepart(Hour,starttime)  > 22) 
	   THEN dateadd(day,  1, starttime)
	   ELSE starttime END) between CONVERT(DateTime,@ReportYear+'/'+CONVERT(NVARCHAR,@ReportMonth)+'/01',101) and CONVERT(DateTime,@ReportYear2+'/'+CONVERT(NVARCHAR,@ReportMonth2)+'/'+CONVERT(NVARCHAR,@ddayy)+' 23:59:59:999')
--  and (CASE 
--	   WHEN (datepart(Hour,starttime)  > 22) 
--	   THEN MONTH(dateadd(day,  1, starttime))
--	   ELSE MONTH(starttime) END) between (@ReportMonth) and (@ReportMonth2)
--and (CASE 
--	   WHEN (datepart(Hour,starttime)  > 22) 
--	   THEN datename(YEAR,dateadd(day,  1, starttime))
--	   ELSE datename(YEAR,starttime) END) between (@ReportYear) and (@ReportYear2)
  
  select min(starttime) 'StartTime'
  ,rundate
  ,RType,avg(Duration) 'Duration'
  ,Case When substring(Package, 1, 7) = 'Column5'
        Then SUBSTRING(Package, 9, 289) 
        Else Package
        End as Package
  from dbo.temp_runtimes
  group by RType,rundate,Package, starttime
  order by starttime
  
   drop table dbo.temp_runtimes
END
END