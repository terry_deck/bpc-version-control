﻿

CREATE PROCEDURE [dbo].[asp_DailyAuditActivityReport]
AS
 
Begin
SELECT     A.ActivityHdrID, DATEADD(hour, -6,a.TimeStamp) TimeStamp, a.App, a.UserID, a.MachineID, a.FunctionalTask,
           a.Source, a.Param, a.Succeeded, a.Duration, a.ReturnMsg,
		   b.ActivityHeaderID, b.ActivityType, b.Field, b.PreviousValue,
		   b.NewValue, a.ActivityKind
FROM    [PROD_COBPCExtra].[dbo].[AuditActivityHdrPROD_COBPC] A, 
              [PROD_COBPCExtra].[dbo].[AuditActivityDetailPROD_COBPC] B 
WHERE    A.[ActivityHdrid] = B.[ActivityHeaderID] 
-- 04-11-2014  T Deck changed the following code to get data from
-- last 24 hours instead of the previous day.  That way we can run this
-- at a constant time and avoid timechange issues
--AND convert(nvarchar(30),a.TimeStamp,6) = CONVERT(nvarchar(30),getdate()-1,6)
AND DATEADD(hour,-6,a.TimeStamp) >= DateAdd(hh, -24, GETDATE())
AND ActivityKind = 'ADMIN'
End

